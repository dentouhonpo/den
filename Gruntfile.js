module.exports = function(grunt) {
    // 各タスクの設定
    grunt.initConfig({
        //package.jsonを取得
        pkg: grunt.file.readJSON('package.json'),


        paths: {
            //開発用Path
            devrk: 'dev/rakuten/',
            devsh: 'dev/shopserve/',
            devya: 'dev/yahoo/',
            devcmn: 'dev/cmn/',

            //アップロード用パス
            relrk: 'rel/rakuten/',
            relsh: 'rel/shopserve/',
            relya: 'rel/yahoo/',
            relcmn: 'rel/cmn/',
            up: 'upload/',

            //データ保管用
            dtspip: 'data/ip_sp_images/',
            dtip: 'data/SceneImage_シーン写真/'
        },

        // Compass
        compass: {
            options: {
                noLineComments: true
            },
            guide_rk: { options: { basePath: '<%= paths.devrk %>styleguide', config: 'config-guide.rb' } },
            frame_rk: { options: { basePath: '<%= paths.devrk %>frameworks', config: 'config.rb' } },
            top_rk: { options: { basePath: '<%= paths.devrk %>', config: 'config.rb' } },
            info_rk: { options: { basePath: '<%= paths.devrk %>info', config: 'config.rb' } },
            interior_rk: { options: { basePath: '<%= paths.devrk %>common/interior', config: 'config.rb' } },
            oversea_rk: { options: { basePath: '<%= paths.devrk %>common/oversea', config: 'config.rb' } },
            common_rk: { options: { basePath: '<%= paths.devrk %>common/', config: 'config.rb' } },
            ip_rk: { options: { basePath: '<%= paths.devrk %>common/ip', config: 'config.rb' } },
            corp_rk: { options: { basePath: '<%= paths.devrk %>corp', config: 'config.rb' } },
            genre_rk: { options: { basePath: '<%= paths.devrk %>genre', config: 'config.rb' } },
            item_rk: { options: { basePath: '<%= paths.devrk %>item', config: 'config.rb' } },
            blog_rk: { options: { basePath: '<%= paths.devrk %>item/blogger', config: 'config.rb' } },
            ev_rk: { options: { basePath: '<%= paths.devrk %>event', config: 'config.rb' } },



            guide_sh: { options: { basePath: '<%= paths.devsh %>styleguide', config: 'config-guide.rb' } },
            frame_sh: { options: { basePath: '<%= paths.devsh %>frameworks', config: 'config.rb' } },
            top_sh: { options: { basePath: '<%= paths.devsh %>', config: 'config.rb' } },
            info_sh: { options: { basePath: '<%= paths.devsh %>info', config: 'config.rb' } },
            interior_sh: { options: { basePath: '<%= paths.devsh %>common/interior', config: 'config.rb' } },
            oversea_sh: { options: { basePath: '<%= paths.devsh %>common/oversea', config: 'config.rb' } },
            common_sh: { options: { basePath: '<%= paths.devsh %>common/', config: 'config.rb' } },
            ip_sh: { options: { basePath: '<%= paths.devsh %>common/ip', config: 'config.rb' } },
            corp_sh: { options: { basePath: '<%= paths.devsh %>corp', config: 'config.rb' } },
            genre_sh: { options: { basePath: '<%= paths.devsh %>genre', config: 'config.rb' } },
            item_sh: { options: { basePath: '<%= paths.devsh %>item', config: 'config.rb' } },
            blog_sh: { options: { basePath: '<%= paths.devsh %>item/blogger', config: 'config.rb' } },
            ev_sh: { options: { basePath: '<%= paths.devsh %>event', config: 'config.rb' } },



            guide_ya: { options: { basePath: '<%= paths.devya %>styleguide', config: 'config-guide.rb' } },
            frame_ya: { options: { basePath: '<%= paths.devya %>frameworks', config: 'config.rb' } },
            top_ya: { options: { basePath: '<%= paths.devya %>', config: 'config.rb' } },
            info_ya: { options: { basePath: '<%= paths.devya %>info', config: 'config.rb' } },
            interior_ya: { options: { basePath: '<%= paths.devya %>common/interior', config: 'config.rb' } },
            oversea_ya: { options: { basePath: '<%= paths.devya %>common/oversea', config: 'config.rb' } },
            common_ya: { options: { basePath: '<%= paths.devya %>common/', config: 'config.rb' } },
            ip_ya: { options: { basePath: '<%= paths.devya %>common/ip', config: 'config.rb' } },
            corp_ya: { options: { basePath: '<%= paths.devya %>corp', config: 'config.rb' } },
            genre_ya: { options: { basePath: '<%= paths.devya %>genre', config: 'config.rb' } },
            item_ya: { options: { basePath: '<%= paths.devya %>item', config: 'config.rb' } },
            blog_ya: { options: { basePath: '<%= paths.devya %>item/blogger', config: 'config.rb' } },
            ev_ya: { options: { basePath: '<%= paths.devya %>event', config: 'config.rb' } },

            //全店舗共通
            info_cmn: { options: { basePath: '<%= paths.devcmn %>info', config: 'config.rb' } },
            cmn_cmn: { options: { basePath: '<%= paths.devcmn %>common', config: 'config.rb' } }
        },

        //画像最適化
        imageoptim: {
            options: {
                jpegMini: false,
                imageAlpha: true,
                quitAfter: false
            }, 

            //楽天画像最適化
            png_top_rk: { expand: true, cwd: '<%= paths.devrk %>img/sprites/', src: ['*.png'] },
            png_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/img/sprites/', src: ['*.png'] },
            png_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/img/sprites/', src: ['*.png'] },
            png_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/img/sprites/', src: ['*.png'] },
            png_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/img/sprites/', src: ['*.png'] },
            png_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/img/sprites/', src: ['*.png'] },
            png_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/img/sprites/', src: ['*.png'] },
            png_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/img/sprites/', src: ['*.png'] },
            png_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/img/sprites/', src: ['*.png'] },
            png_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/img/sprites/', src: ['*.png'] },
            png_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/img/sprites/', src: ['*.png'] },

            jpg_top_rk: { expand: true, cwd: '<%= paths.devrk %>img/', src: ['*.jpg'] },
            jpg_top_header_rk: { expand: true, cwd: '<%= paths.devrk %>img/header', src: ['*.jpg'] },
            jpg_top_footer_rk: { expand: true, cwd: '<%= paths.devrk %>img/footer', src: ['*.jpg'] },
            jpg_top_leftnav_rk: { expand: true, cwd: '<%= paths.devrk %>img/leftnav', src: ['*.jpg'] },

            jpg_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/img/', src: ['*.jpg'] },
            jpg_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/img/', src: ['*.jpg'] },
            jpg_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/img/', src: ['*.jpg'] },
            jpg_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/img/', src: ['*.jpg'] },
            jpg_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/img/', src: ['*.jpg'] },
            jpg_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/img/', src: ['*.jpg'] },
            jpg_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/img/', src: ['*.jpg'] },
            jpg_genre_month_rk: { expand: true, cwd: '<%= paths.devrk %>genre/month/img/', src: ['*.jpg'] },
            jpg_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/img/', src: ['*.jpg'] },
            jpg_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/img/', src: ['*.jpg'] },
            jpg_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/img/', src: ['*.jpg'] },



            //shopserve画像最適化
            png_top_sh: { expand: true, cwd: '<%= paths.devsh %>img/sprites/', src: ['*.png'] },
            png_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/img/sprites/', src: ['*.png'] },
            png_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/img/sprites/', src: ['*.png'] },
            png_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/img/sprites/', src: ['*.png'] },
            png_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/img/sprites/', src: ['*.png'] },
            png_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/img/sprites/', src: ['*.png'] },
            png_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/img/sprites/', src: ['*.png'] },
            png_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/img/sprites/', src: ['*.png'] },
            png_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/img/sprites/', src: ['*.png'] },
            png_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/img/sprites/', src: ['*.png'] },
            png_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/img/sprites/', src: ['*.png'] },

            jpg_top_sh: { expand: true, cwd: '<%= paths.devsh %>img/', src: ['*.jpg'] },
            jpg_top_header_sh: { expand: true, cwd: '<%= paths.devsh %>img/header', src: ['*.jpg'] },
            jpg_top_footer_sh: { expand: true, cwd: '<%= paths.devsh %>img/footer', src: ['*.jpg'] },
            jpg_top_leftnav_sh: { expand: true, cwd: '<%= paths.devsh %>img/leftnav', src: ['*.jpg'] },

            jpg_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/img/', src: ['*.jpg'] },
            jpg_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/img/', src: ['*.jpg'] },
            jpg_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/img/', src: ['*.jpg'] },
            jpg_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/img/', src: ['*.jpg'] },
            jpg_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/img/', src: ['*.jpg'] },
            jpg_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/img/', src: ['*.jpg'] },
            jpg_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/img/', src: ['*.jpg'] },
            jpg_genre_month_sh: { expand: true, cwd: '<%= paths.devsh %>genre/month/img/', src: ['*.jpg'] },
            jpg_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/img/', src: ['*.jpg'] },
            jpg_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/img/', src: ['*.jpg'] },
            jpg_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/img/', src: ['*.jpg'] },


            //yahoo画像最適化
            png_top_ya: { expand: true, cwd: '<%= paths.devya %>img/sprites/', src: ['*.png'] },
            png_top_header_ya: { expand: true, cwd: '<%= paths.devya %>img/header/sprites/', src: ['*.png'] },
            png_top_footer_ya: { expand: true, cwd: '<%= paths.devya %>img/footer/sprites/', src: ['*.png'] },
            png_top_leftnav_ya: { expand: true, cwd: '<%= paths.devya %>img/leftnav/sprites/', src: ['*.png'] },

            png_info_ya: { expand: true, cwd: '<%= paths.devya %>info/img/sprites/', src: ['*.png'] },
            png_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/img/sprites/', src: ['*.png'] },
            png_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/img/sprites/', src: ['*.png'] },
            png_common_ya: { expand: true, cwd: '<%= paths.devya %>common/img/sprites/', src: ['*.png'] },
            png_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/img/sprites/', src: ['*.png'] },
            png_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/img/sprites/', src: ['*.png'] },
            png_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/img/sprites/', src: ['*.png'] },
            png_item_ya: { expand: true, cwd: '<%= paths.devya %>item/img/sprites/', src: ['*.png'] },
            png_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/img/sprites/', src: ['*.png'] },
            png_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/img/sprites/', src: ['*.png'] },

            jpg_top_ya: { expand: true, cwd: '<%= paths.devya %>img/', src: ['*.jpg'] },
            jpg_top_header_ya: { expand: true, cwd: '<%= paths.devya %>img/header', src: ['*.jpg'] },
            jpg_top_footer_ya: { expand: true, cwd: '<%= paths.devya %>img/footer', src: ['*.jpg'] },
            jpg_top_leftnav_ya: { expand: true, cwd: '<%= paths.devya %>img/leftnav', src: ['*.jpg'] },

            jpg_top_header_ya: { expand: true, cwd: '<%= paths.devya %>img/header/', src: ['*.jpg'] },
            jpg_top_footer_ya: { expand: true, cwd: '<%= paths.devya %>img/footer/', src: ['*.jpg'] },
            jpg_top_leftnav_ya: { expand: true, cwd: '<%= paths.devya %>img/leftnav/', src: ['*.jpg'] },

            jpg_info_ya: { expand: true, cwd: '<%= paths.devya %>info/img/', src: ['*.jpg'] },
            jpg_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/img/', src: ['*.jpg'] },
            jpg_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/img/', src: ['*.jpg'] },
            jpg_common_ya: { expand: true, cwd: '<%= paths.devya %>common/img/', src: ['*.jpg'] },
            jpg_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/img/', src: ['*.jpg'] },
            jpg_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/img/', src: ['*.jpg'] },
            jpg_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/img/', src: ['*.jpg'] },
            jpg_genre_month_ya: { expand: true, cwd: '<%= paths.devya %>genre/month/img/', src: ['*.jpg'] },
            jpg_item_ya: { expand: true, cwd: '<%= paths.devya %>item/img/', src: ['*.jpg'] },
            jpg_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/img/', src: ['*.jpg'] },
            jpg_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/img/', src: ['*.jpg'] },

            //共通フォルダ
            png_info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/img/sprites/', src: ['*.png'] },
            jpg_info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/img/', src: ['*.jpg'] },
            png_cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/img/sprites/', src: ['*.png'] },
            jpg_cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/img/', src: ['*.jpg'] }
        },

        //gzip圧縮
        compress: {
            options: {
                mode: 'gzip'
            },
            //楽天用gzipファイル圧縮
            css_frame_rk: { expand: true, cwd: '<%= paths.relrk %>frameworks/css/', src: '*.css', dest: '<%= paths.relrk %>frameworks/css/', ext: '.css.gz' },
            css_top_rk: { expand: true, cwd: '<%= paths.relrk %>css/', src: '*.css', dest: '<%= paths.relrk %>css/', ext: '.css.gz' },
            css_info_rk: { expand: true, cwd: '<%= paths.relrk %>info/css/', src: '*.css', dest: '<%= paths.relrk %>info/css/', ext: '.css.gz' },
            css_interior_rk: { expand: true, cwd: '<%= paths.relrk %>common/interior/css/', src: '*.css', dest: '<%= paths.relrk %>common/interior/css/', ext: '.css.gz' },
            css_oversea_rk: { expand: true, cwd: '<%= paths.relrk %>common/oversea/css/', src: '*.css', dest: '<%= paths.relrk %>common/oversea/css/', ext: '.css.gz' },
            css_common_rk: { expand: true, cwd: '<%= paths.relrk %>common/css/', src: '*.css', dest: '<%= paths.relrk %>common/css/', ext: '.css.gz' },
            css_ip_rk: { expand: true, cwd: '<%= paths.relrk %>common/ip/css/', src: '*.css', dest: '<%= paths.relrk %>common/ip/css/', ext: '.css.gz' },
            css_corp_rk: { expand: true, cwd: '<%= paths.relrk %>corp/css/', src: '*.css', dest: '<%= paths.relrk %>corp/css/', ext: '.css.gz' },
            css_genre_rk: { expand: true, cwd: '<%= paths.relrk %>genre/css/', src: '*.css', dest: '<%= paths.relrk %>genre/css/', ext: '.css.gz' },
            css_item_rk: { expand: true, cwd: '<%= paths.relrk %>item/css/', src: '*.css', dest: '<%= paths.relrk %>item/css/', ext: '.css.gz' },
            css_blog_rk: { expand: true, cwd: '<%= paths.relrk %>item/blogger/css/', src: '*.css', dest: '<%= paths.relrk %>item/blogger/css/', ext: '.css.gz' },
            css_ev_rk: { expand: true, cwd: '<%= paths.relrk %>event/css/', src: '*.css', dest: '<%= paths.relrk %>event/css/', ext: '.css.gz' },

            js_frame_rk: { expand: true, cwd: '<%= paths.relrk %>frameworks/js/', src: '*.js', dest: '<%= paths.relrk %>frameworks/js/', ext: '.js.gz' },
            js_top_rk: { expand: true, cwd: '<%= paths.relrk %>js/', src: '*.js', dest: '<%= paths.relrk %>js/', ext: '.js.gz' },
            js_info_rk: { expand: true, cwd: '<%= paths.relrk %>info/js/', src: '*.js', dest: '<%= paths.relrk %>info/js/', ext: '.js.gz' },
            js_interior_rk: { expand: true, cwd: '<%= paths.relrk %>common/interior/js/', src: '*.js', dest: '<%= paths.relrk %>common/interior/js/', ext: '.js.gz' },
            js_oversea_rk: { expand: true, cwd: '<%= paths.relrk %>common/oversea/js/', src: '*.js', dest: '<%= paths.relrk %>common/oversea/js/', ext: '.js.gz' },
            js_common_rk: { expand: true, cwd: '<%= paths.relrk %>common/js/', src: '*.js', dest: '<%= paths.relrk %>common/js/', ext: '.js.gz' },
            js_ip_rk: { expand: true, cwd: '<%= paths.relrk %>common/ip/js/', src: '*.js', dest: '<%= paths.relrk %>common/ip/js/', ext: '.js.gz' },
            js_corp_rk: { expand: true, cwd: '<%= paths.relrk %>corp/js/', src: '*.js', dest: '<%= paths.relrk %>corp/js/', ext: '.js.gz' },
            js_genre_rk: { expand: true, cwd: '<%= paths.relrk %>genre/js/', src: '*.js', dest: '<%= paths.relrk %>genre/js/', ext: '.js.gz' },
            js_item_rk: { expand: true, cwd: '<%= paths.relrk %>item/js/', src: '*.js', dest: '<%= paths.relrk %>item/js/', ext: '.js.gz' },
            js_blog_rk: { expand: true, cwd: '<%= paths.relrk %>item/blogger/js/', src: '*.js', dest: '<%= paths.relrk %>item/blogger/js/', ext: '.js.gz' },
            js_ev_rk: { expand: true, cwd: '<%= paths.relrk %>event/js/', src: '*.js', dest: '<%= paths.relrk %>event/js/', ext: '.js.gz' },





            //shopservegzipファイル圧縮
            css_frame_sh: { expand: true, cwd: '<%= paths.relsh %>frameworks/css/', src: '*.css', dest: '<%= paths.relsh %>frameworks/css/', ext: '.css.gz' },
            css_top_sh: { expand: true, cwd: '<%= paths.relsh %>css/', src: '*.css', dest: '<%= paths.relsh %>css/', ext: '.css.gz' },
            css_info_sh: { expand: true, cwd: '<%= paths.relsh %>info/css/', src: '*.css', dest: '<%= paths.relsh %>info/css/', ext: '.css.gz' },
            css_interior_sh: { expand: true, cwd: '<%= paths.relsh %>common/interior/css/', src: '*.css', dest: '<%= paths.relsh %>common/interior/css/', ext: '.css.gz' },
            css_oversea_sh: { expand: true, cwd: '<%= paths.relsh %>common/oversea/css/', src: '*.css', dest: '<%= paths.relsh %>common/oversea/css/', ext: '.css.gz' },
            css_common_sh: { expand: true, cwd: '<%= paths.relsh %>common/css/', src: '*.css', dest: '<%= paths.relsh %>common/css/', ext: '.css.gz' },
            css_ip_sh: { expand: true, cwd: '<%= paths.relsh %>common/ip/css/', src: '*.css', dest: '<%= paths.relsh %>common/ip/css/', ext: '.css.gz' },
            css_corp_sh: { expand: true, cwd: '<%= paths.relsh %>corp/css/', src: '*.css', dest: '<%= paths.relsh %>corp/css/', ext: '.css.gz' },
            css_genre_sh: { expand: true, cwd: '<%= paths.relsh %>genre/css/', src: '*.css', dest: '<%= paths.relsh %>genre/css/', ext: '.css.gz' },
            css_item_sh: { expand: true, cwd: '<%= paths.relsh %>item/css/', src: '*.css', dest: '<%= paths.relsh %>item/css/', ext: '.css.gz' },
            css_blog_sh: { expand: true, cwd: '<%= paths.relsh %>item/blogger/css/', src: '*.css', dest: '<%= paths.relsh %>item/blogger/css/', ext: '.css.gz' },
            css_ev_sh: { expand: true, cwd: '<%= paths.relsh %>event/css/', src: '*.css', dest: '<%= paths.relsh %>event/css/', ext: '.css.gz' },

            js_frame_sh: { expand: true, cwd: '<%= paths.relsh %>frameworks/js/', src: '*.js', dest: '<%= paths.relsh %>frameworks/js/', ext: '.js.gz' },
            js_top_sh: { expand: true, cwd: '<%= paths.relsh %>js/', src: '*.js', dest: '<%= paths.relsh %>js/', ext: '.js.gz' },
            js_info_sh: { expand: true, cwd: '<%= paths.relsh %>info/js/', src: '*.js', dest: '<%= paths.relsh %>info/js/', ext: '.js.gz' },
            js_interior_sh: { expand: true, cwd: '<%= paths.relsh %>common/interior/js/', src: '*.js', dest: '<%= paths.relsh %>common/interior/js/', ext: '.js.gz' },
            js_oversea_sh: { expand: true, cwd: '<%= paths.relsh %>common/oversea/js/', src: '*.js', dest: '<%= paths.relsh %>common/oversea/js/', ext: '.js.gz' },
            js_common_sh: { expand: true, cwd: '<%= paths.relsh %>common/js/', src: '*.js', dest: '<%= paths.relsh %>common/js/', ext: '.js.gz' },
            js_ip_sh: { expand: true, cwd: '<%= paths.relsh %>common/ip/js/', src: '*.js', dest: '<%= paths.relsh %>common/ip/js/', ext: '.js.gz' },
            js_corp_sh: { expand: true, cwd: '<%= paths.relsh %>corp/js/', src: '*.js', dest: '<%= paths.relsh %>corp/js/', ext: '.js.gz' },
            js_genre_sh: { expand: true, cwd: '<%= paths.relsh %>genre/js/', src: '*.js', dest: '<%= paths.relsh %>genre/js/', ext: '.js.gz' },
            js_item_sh: { expand: true, cwd: '<%= paths.relsh %>item/js/', src: '*.js', dest: '<%= paths.relsh %>item/js/', ext: '.js.gz' },
            js_blog_sh: { expand: true, cwd: '<%= paths.relsh %>item/blogger/js/', src: '*.js', dest: '<%= paths.relsh %>item/blogger/js/', ext: '.js.gz' },
            js_ev_sh: { expand: true, cwd: '<%= paths.relsh %>event/js/', src: '*.js', dest: '<%= paths.relsh %>event/js/', ext: '.js.gz' },



            //yahoogzip圧縮
            css_frame_ya: { expand: true, cwd: '<%= paths.relya %>frameworks/css/', src: '*.css', dest: '<%= paths.relya %>frameworks/css/', ext: '.css.gz' },
            css_top_ya: { expand: true, cwd: '<%= paths.relya %>css/', src: '*.css', dest: '<%= paths.relya %>css/', ext: '.css.gz' },
            css_info_ya: { expand: true, cwd: '<%= paths.relya %>info/css/', src: '*.css', dest: '<%= paths.relya %>info/css/', ext: '.css.gz' },
            css_interior_ya: { expand: true, cwd: '<%= paths.relya %>common/interior/css/', src: '*.css', dest: '<%= paths.relya %>common/interior/css/', ext: '.css.gz' },
            css_oversea_ya: { expand: true, cwd: '<%= paths.relya %>common/oversea/css/', src: '*.css', dest: '<%= paths.relya %>common/oversea/css/', ext: '.css.gz' },
            css_common_ya: { expand: true, cwd: '<%= paths.relya %>common/css/', src: '*.css', dest: '<%= paths.relya %>common/css/', ext: '.css.gz' },
            css_ip_ya: { expand: true, cwd: '<%= paths.relya %>common/ip/css/', src: '*.css', dest: '<%= paths.relya %>common/ip/css/', ext: '.css.gz' },
            css_corp_ya: { expand: true, cwd: '<%= paths.relya %>corp/css/', src: '*.css', dest: '<%= paths.relya %>corp/css/', ext: '.css.gz' },
            css_genre_ya: { expand: true, cwd: '<%= paths.relya %>genre/css/', src: '*.css', dest: '<%= paths.relya %>genre/css/', ext: '.css.gz' },
            css_item_ya: { expand: true, cwd: '<%= paths.relya %>item/css/', src: '*.css', dest: '<%= paths.relya %>item/css/', ext: '.css.gz' },
            css_blog_ya: { expand: true, cwd: '<%= paths.relya %>item/blogger/css/', src: '*.css', dest: '<%= paths.relya %>item/blogger/css/', ext: '.css.gz' },
            css_ev_ya: { expand: true, cwd: '<%= paths.relya %>event/css/', src: '*.css', dest: '<%= paths.relya %>event/css/', ext: '.css.gz' },

            js_frame_ya: { expand: true, cwd: '<%= paths.relya %>frameworks/js/', src: '*.js', dest: '<%= paths.relya %>frameworks/js/', ext: '.js.gz' },
            js_top_ya: { expand: true, cwd: '<%= paths.relya %>js/', src: '*.js', dest: '<%= paths.relya %>js/', ext: '.js.gz' },
            js_info_ya: { expand: true, cwd: '<%= paths.relya %>info/js/', src: '*.js', dest: '<%= paths.relya %>info/js/', ext: '.js.gz' },
            js_interior_ya: { expand: true, cwd: '<%= paths.relya %>common/interior/js/', src: '*.js', dest: '<%= paths.relya %>common/interior/js/', ext: '.js.gz' },
            js_oversea_ya: { expand: true, cwd: '<%= paths.relya %>common/oversea/js/', src: '*.js', dest: '<%= paths.relya %>common/oversea/js/', ext: '.js.gz' },
            js_common_ya: { expand: true, cwd: '<%= paths.relya %>common/js/', src: '*.js', dest: '<%= paths.relya %>common/js/', ext: '.js.gz' },
            js_ip_ya: { expand: true, cwd: '<%= paths.relya %>common/ip/js/', src: '*.js', dest: '<%= paths.relya %>common/ip/js/', ext: '.js.gz' },
            js_corp_ya: { expand: true, cwd: '<%= paths.relya %>corp/js/', src: '*.js', dest: '<%= paths.relya %>corp/js/', ext: '.js.gz' },
            js_genre_ya: { expand: true, cwd: '<%= paths.relya %>genre/js/', src: '*.js', dest: '<%= paths.relya %>genre/js/', ext: '.js.gz' },
            js_item_ya: { expand: true, cwd: '<%= paths.relya %>item/js/', src: '*.js', dest: '<%= paths.relya %>item/js/', ext: '.js.gz' },
            js_blog_ya: { expand: true, cwd: '<%= paths.relya %>item/blogger/js/', src: '*.js', dest: '<%= paths.relya %>item/blogger/js/', ext: '.js.gz' },
            js_ev_ya: { expand: true, cwd: '<%= paths.relya %>event/js/', src: '*.js', dest: '<%= paths.relya %>event/js/', ext: '.js.gz' },

            //共通フォルダ
            css_info_cmn: { expand: true, cwd: '<%= paths.relcmn %>info/css/', src: '*.css', dest: '<%= paths.relcmn %>info/css/', ext: '.css.gz' },
            js_info_cmn: { expand: true, cwd: '<%= paths.relcmn %>info/js/', src: '*.js', dest: '<%= paths.relcmn %>info/js/', ext: '.js.gz' },
            css_cmn_cmn: { expand: true, cwd: '<%= paths.relcmn %>common/css/', src: '*.css', dest: '<%= paths.relcmn %>common/css/', ext: '.css.gz' },
            js_cmn_cmn: { expand: true, cwd: '<%= paths.relcmn %>common/js/', src: '*.js', dest: '<%= paths.relcmn %>common/js/', ext: '.js.gz' }
        },

        //ファイルのコピー
        copy: {
            css_frame_rk: { expand: true, cwd: '<%= paths.devrk %>frameworks/css/', src: '*.css', dest: '<%= paths.relrk %>frameworks/css/' },
            css_top_rk: { expand: true, cwd: '<%= paths.devrk %>css/', src: '*.css', dest: '<%= paths.relrk %>css/' },
            css_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/css/', src: '*.css', dest: '<%= paths.relrk %>info/css/' },
            css_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/css/', src: '*.css', dest: '<%= paths.relrk %>common/interior/css/' },
            css_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/css/', src: '*.css', dest: '<%= paths.relrk %>common/oversea/css/' },
            css_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/css/', src: '*.css', dest: '<%= paths.relrk %>common/css/' },
            css_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/css/', src: '*.css', dest: '<%= paths.relrk %>common/ip/css/' },
            css_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/css/', src: '*.css', dest: '<%= paths.relrk %>corp/css/' },
            css_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/css/', src: '*.css', dest: '<%= paths.relrk %>genre/css/' },
            css_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/css/', src: '*.css', dest: '<%= paths.relrk %>item/css/' },
            css_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/css/', src: '*.css', dest: '<%= paths.relrk %>item/blogger/css/' },
            css_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/css/', src: '*.css', dest: '<%= paths.relrk %>event/css/' },

            js_frame_rk: { expand: true, cwd: '<%= paths.devrk %>frameworks/js/', src: '*.js', dest: '<%= paths.relrk %>frameworks/js/' },
            js_top_rk: { expand: true, cwd: '<%= paths.devrk %>js/', src: '*.js', dest: '<%= paths.relrk %>js/' },
            js_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/js/', src: '*.js', dest: '<%= paths.relrk %>info/js/' },
            js_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/js/', src: '*.js', dest: '<%= paths.relrk %>common/interior/js/' },
            js_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/js/', src: '*.js', dest: '<%= paths.relrk %>common/oversea/js/' },
            js_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/js/', src: '*.js', dest: '<%= paths.relrk %>common/js/' },
            js_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/js/', src: '*.js', dest: '<%= paths.relrk %>common/ip/js/' },
            js_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/js/', src: '*.js', dest: '<%= paths.relrk %>corp/js/' },
            js_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/js/', src: '*.js', dest: '<%= paths.relrk %>genre/js/' },
            js_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/js/', src: '*.js', dest: '<%= paths.relrk %>item/js/' },
            js_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/js/', src: '*.js', dest: '<%= paths.relrk %>item/blogger/js/' },
            js_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/js/', src: '*.js', dest: '<%= paths.relrk %>event/js/' },


            jpg_top_rk: { expand: true, cwd: '<%= paths.devrk %>img/', src: '*.jpg', dest: '<%= paths.relrk %>img/' },
            jpg_top_header_rk: { expand: true, cwd: '<%= paths.devrk %>img/header/', src: '*.jpg', dest: '<%= paths.relrk %>img/header/' },
            jpg_top_footer_rk: { expand: true, cwd: '<%= paths.devrk %>img/footer/', src: '*.jpg', dest: '<%= paths.relrk %>img/footer/' },
            jpg_top_leftnav_rk: { expand: true, cwd: '<%= paths.devrk %>img/leftnav/', src: '*.jpg', dest: '<%= paths.relrk %>img/leftnav/' },
            jpg_ipcopy_rk_ip_ya: { expand: true, cwd: '<%= paths.relrk %>item/img/', src: 'ip*.jpg', dest: '<%= paths.relya %>item/img/' },
            jpg_ipcopy_rk_ip_sh: { expand: true, cwd: '<%= paths.relrk %>item/img/', src: 'ip*.jpg', dest: '<%= paths.relsh %>item/img/' },
            jpg_ipcopy_rk_sp: { expand: true, cwd: '<%= paths.relrk %>item/img/', src: 'sp*.jpg', dest: '<%= paths.relya %>item/img/' },
            jpg_ipcopy_dt_sp: { expand: true, cwd: '<%= paths.relrk %>item/img/', src: 'sp*.jpg', dest: '<%= paths.dtspip %>' },
            jpg_ipcopy_dt_ip: { expand: true, cwd: '<%= paths.relrk %>item/img/', src: 'ip*.jpg', dest: '<%= paths.dtip %>' },

            jpg_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/img/', src: '*.jpg', dest: '<%= paths.relrk %>info/img/' },
            jpg_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/img/', src: '*.jpg', dest: '<%= paths.relrk %>common/interior/img/' },
            jpg_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/img/', src: '*.jpg', dest: '<%= paths.relrk %>common/oversea/img/' },
            jpg_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/img/', src: '*.jpg', dest: '<%= paths.relrk %>common/img/' },
            jpg_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/img/', src: '*.jpg', dest: '<%= paths.relrk %>common/ip/img/' },
            jpg_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/img/', src: '*.jpg', dest: '<%= paths.relrk %>corp/img/' },
            jpg_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/img/', src: '*.jpg', dest: '<%= paths.relrk %>genre/img/' },
            jpg_genre_month_rk: { expand: true, cwd: '<%= paths.devrk %>genre/month/img/', src: '*.jpg', dest: '<%= paths.relrk %>genre/month/img/' },
            jpg_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/img/', src: '*.jpg', dest: '<%= paths.relrk %>item/img/' },
            jpg_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/img/', src: '*.jpg', dest: '<%= paths.relrk %>item/blogger/img/' },
            jpg_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/img/', src: '*.jpg', dest: '<%= paths.relrk %>event/img/' },




            //shopserve
            css_frame_sh: { expand: true, cwd: '<%= paths.devsh %>frameworks/css/', src: '*.css', dest: '<%= paths.relsh %>frameworks/css/' },
            css_top_sh: { expand: true, cwd: '<%= paths.devsh %>css/', src: '*.css', dest: '<%= paths.relsh %>css/' },
            css_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/css/', src: '*.css', dest: '<%= paths.relsh %>info/css/' },
            css_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/css/', src: '*.css', dest: '<%= paths.relsh %>common/interior/css/' },
            css_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/css/', src: '*.css', dest: '<%= paths.relsh %>common/oversea/css/' },
            css_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/css/', src: '*.css', dest: '<%= paths.relsh %>common/css/' },
            css_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/css/', src: '*.css', dest: '<%= paths.relsh %>common/ip/css/' },
            css_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/css/', src: '*.css', dest: '<%= paths.relsh %>corp/css/' },
            css_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/css/', src: '*.css', dest: '<%= paths.relsh %>genre/css/' },
            css_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/css/', src: '*.css', dest: '<%= paths.relsh %>item/css/' },
            css_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/css/', src: '*.css', dest: '<%= paths.relsh %>item/blogger/css/' },
            css_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/css/', src: '*.css', dest: '<%= paths.relsh %>event/css/' },

            js_frame_sh: { expand: true, cwd: '<%= paths.devsh %>frameworks/js/', src: '*.js', dest: '<%= paths.relsh %>frameworks/js/' },
            js_top_sh: { expand: true, cwd: '<%= paths.devsh %>js/', src: '*.js', dest: '<%= paths.relsh %>js/' },
            js_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/js/', src: '*.js', dest: '<%= paths.relsh %>info/js/' },
            js_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/js/', src: '*.js', dest: '<%= paths.relsh %>common/interior/js/' },
            js_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/js/', src: '*.js', dest: '<%= paths.relsh %>common/oversea/js/' },
            js_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/js/', src: '*.js', dest: '<%= paths.relsh %>common/js/' },
            js_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/js/', src: '*.js', dest: '<%= paths.relsh %>common/ip/js/' },
            js_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/js/', src: '*.js', dest: '<%= paths.relsh %>corp/js/' },
            js_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/js/', src: '*.js', dest: '<%= paths.relsh %>genre/js/' },
            js_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/js/', src: '*.js', dest: '<%= paths.relsh %>item/js/' },
            js_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/js/', src: '*.js', dest: '<%= paths.relsh %>item/blogger/js/' },
            js_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/js/', src: '*.js', dest: '<%= paths.relsh %>event/js/' },


            jpg_top_sh: { expand: true, cwd: '<%= paths.devsh %>img/', src: '*.jpg', dest: '<%= paths.relsh %>img/' },
            jpg_top_header_sh: { expand: true, cwd: '<%= paths.devsh %>img/header/', src: '*.jpg', dest: '<%= paths.relsh %>img/header/' },
            jpg_top_footer_sh: { expand: true, cwd: '<%= paths.devsh %>img/footer/', src: '*.jpg', dest: '<%= paths.relsh %>img/footer/' },
            jpg_top_leftnav_sh: { expand: true, cwd: '<%= paths.devsh %>img/leftnav/', src: '*.jpg', dest: '<%= paths.relsh %>img/leftnav/' },

            jpg_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/img/', src: '*.jpg', dest: '<%= paths.relsh %>info/img/' },
            jpg_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/img/', src: '*.jpg', dest: '<%= paths.relsh %>common/interior/img/' },
            jpg_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/img/', src: '*.jpg', dest: '<%= paths.relsh %>common/oversea/img/' },
            jpg_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/img/', src: '*.jpg', dest: '<%= paths.relsh %>common/img/' },
            jpg_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/img/', src: '*.jpg', dest: '<%= paths.relsh %>common/ip/img/' },
            jpg_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/img/', src: '*.jpg', dest: '<%= paths.relsh %>corp/img/' },
            jpg_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/img/', src: '*.jpg', dest: '<%= paths.relsh %>genre/img/' },
            jpg_genre_month_sh: { expand: true, cwd: '<%= paths.devsh %>genre/month/img/', src: '*.jpg', dest: '<%= paths.relsh %>genre/month/img/' },
            jpg_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/img/', src: '*.jpg', dest: '<%= paths.relsh %>item/img/' },
            jpg_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/img/', src: '*.jpg', dest: '<%= paths.relsh %>item/blogger/img/' },
            jpg_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/img/', src: '*.jpg', dest: '<%= paths.relsh %>event/img/' },




            //yahoo
            css_frame_ya: { expand: true, cwd: '<%= paths.devya %>frameworks/css/', src: '*.css', dest: '<%= paths.relya %>frameworks/css/' },
            css_top_ya: { expand: true, cwd: '<%= paths.devya %>css/', src: '*.css', dest: '<%= paths.relya %>css/' },
            css_info_ya: { expand: true, cwd: '<%= paths.devya %>info/css/', src: '*.css', dest: '<%= paths.relya %>info/css/' },
            css_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/css/', src: '*.css', dest: '<%= paths.relya %>common/interior/css/' },
            css_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/css/', src: '*.css', dest: '<%= paths.relya %>common/oversea/css/' },
            css_common_ya: { expand: true, cwd: '<%= paths.devya %>common/css/', src: '*.css', dest: '<%= paths.relya %>common/css/' },
            css_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/css/', src: '*.css', dest: '<%= paths.relya %>common/ip/css/' },
            css_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/css/', src: '*.css', dest: '<%= paths.relya %>corp/css/' },
            css_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/css/', src: '*.css', dest: '<%= paths.relya %>genre/css/' },
            css_item_ya: { expand: true, cwd: '<%= paths.devya %>item/css/', src: '*.css', dest: '<%= paths.relya %>item/css/' },
            css_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/css/', src: '*.css', dest: '<%= paths.relya %>item/blogger/css/' },
            css_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/css/', src: '*.css', dest: '<%= paths.relya %>event/css/' },

            js_frame_ya: { expand: true, cwd: '<%= paths.devya %>frameworks/js/', src: '*.js', dest: '<%= paths.relya %>frameworks/js/' },
            js_top_ya: { expand: true, cwd: '<%= paths.devya %>js/', src: '*.js', dest: '<%= paths.relya %>js/' },
            js_info_ya: { expand: true, cwd: '<%= paths.devya %>info/js/', src: '*.js', dest: '<%= paths.relya %>info/js/' },
            js_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/js/', src: '*.js', dest: '<%= paths.relya %>common/interior/js/' },
            js_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/js/', src: '*.js', dest: '<%= paths.relya %>common/oversea/js/' },
            js_common_ya: { expand: true, cwd: '<%= paths.devya %>common/js/', src: '*.js', dest: '<%= paths.relya %>common/js/' },
            js_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/js/', src: '*.js', dest: '<%= paths.relya %>common/ip/js/' },
            js_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/js/', src: '*.js', dest: '<%= paths.relya %>corp/js/' },
            js_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/js/', src: '*.js', dest: '<%= paths.relya %>genre/js/' },
            js_item_ya: { expand: true, cwd: '<%= paths.devya %>item/js/', src: '*.js', dest: '<%= paths.relya %>item/js/' },
            js_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/js/', src: '*.js', dest: '<%= paths.relya %>item/blogger/js/' },
            js_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/js/', src: '*.js', dest: '<%= paths.relya %>event/js/' },


            jpg_top_ya: { expand: true, cwd: '<%= paths.devya %>img/', src: '*.jpg', dest: '<%= paths.relya %>img/' },
            jpg_top_header_ya: { expand: true, cwd: '<%= paths.devya %>img/header/', src: '*.jpg', dest: '<%= paths.relya %>img/header/' },
            jpg_top_footer_ya: { expand: true, cwd: '<%= paths.devya %>img/footer/', src: '*.jpg', dest: '<%= paths.relya %>img/footer/' },
            jpg_top_leftnav_ya: { expand: true, cwd: '<%= paths.devya %>img/leftnav/', src: '*.jpg', dest: '<%= paths.relya %>img/leftnav/' },

            jpg_info_ya: { expand: true, cwd: '<%= paths.devya %>info/img/', src: '*.jpg', dest: '<%= paths.relya %>info/img/' },
            jpg_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/img/', src: '*.jpg', dest: '<%= paths.relya %>common/interior/img/' },
            jpg_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/img/', src: '*.jpg', dest: '<%= paths.relya %>common/oversea/img/' },
            jpg_common_ya: { expand: true, cwd: '<%= paths.devya %>common/img/', src: '*.jpg', dest: '<%= paths.relya %>common/img/' },
            jpg_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/img/', src: '*.jpg', dest: '<%= paths.relya %>common/ip/img/' },
            jpg_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/img/', src: '*.jpg', dest: '<%= paths.relya %>corp/img/' },
            jpg_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/img/', src: '*.jpg', dest: '<%= paths.relya %>genre/img/' },
            jpg_genre_month_ya: { expand: true, cwd: '<%= paths.devya %>genre/month/img/', src: '*.jpg', dest: '<%= paths.relya %>genre/month/img/' },
            jpg_item_ya: { expand: true, cwd: '<%= paths.devya %>item/img/', src: '*.jpg', dest: '<%= paths.relya %>item/img/' },
            jpg_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/img/', src: '*.jpg', dest: '<%= paths.relya %>item/blogger/img/' },
            jpg_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/img/', src: '*.jpg', dest: '<%= paths.relya %>event/img/' },


            //共通フォルダ
            css_info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/css/', src: '*.css', dest: '<%= paths.relcmn %>info/css/' },
            js_info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/js/', src: '*.js', dest: '<%= paths.relcmn %>info/js/' },
            jpg_info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/img/', src: '*.jpg', dest: '<%= paths.relcmn %>info/img/' },
            css_cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/css/', src: '*.css', dest: '<%= paths.relcmn %>common/css/' },
            js_cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/js/', src: '*.js', dest: '<%= paths.relcmn %>common/js/' },
            jpg_cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/img/', src: '*.jpg', dest: '<%= paths.relcmn %>common/img/' },



            //出力したpng画像を開発環境のフォルダにコピー  Compass使用時に利用する
            sprite_devrk: {
                files: [
                    { expand: true, cwd: '<%= paths.devrk %>', src: '*.png', dest: '<%= paths.devrk %>img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>info/', src: '*.png', dest: '<%= paths.devrk %>info/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>common/interior/', src: '*.png', dest: '<%= paths.devrk %>common/interior/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>common/oversea/', src: '*.png', dest: '<%= paths.devrk %>common/oversea/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>common/', src: '*.png', dest: '<%= paths.devrk %>common/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>common/ip/', src: '*.png', dest: '<%= paths.devrk %>common/ip/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>corp/', src: '*.png', dest: '<%= paths.devrk %>corp/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>genre/', src: '*.png', dest: '<%= paths.devrk %>genre/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>item/', src: '*.png', dest: '<%= paths.devrk %>item/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>item/blogger/', src: '*.png', dest: '<%= paths.devrk %>item/blogger/img/sprites/' },
                    { expand: true, cwd: '<%= paths.devrk %>event/', src: '*.png', dest: '<%= paths.devrk %>event/imgsprites/' }
                ]
            },

            //出力したpng画像を本番環境のフォルダにコピー
            png_top_rk: { expand: true, cwd: '<%= paths.devrk %>img/sprites/', src: '*.png', dest: '<%= paths.relrk %>img/sprites/' },
            png_info_rk: { expand: true, cwd: '<%= paths.devrk %>info/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>info/img/sprites/' },
            png_interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>common/interior/img/sprites/' },
            png_oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>common/oversea/img/sprites/' },
            png_common_rk: { expand: true, cwd: '<%= paths.devrk %>common/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>common/img/sprites/' },
            png_ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>common/ip/img/sprites/' },
            png_corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>corp/img/sprites/' },
            png_genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>genre/img/sprites/' },
            png_item_rk: { expand: true, cwd: '<%= paths.devrk %>item/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>item/img/sprites/' },
            png_blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>item/blogger/img/sprites/' },
            png_ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/img/sprites/', src: '*.png', dest: '<%= paths.relrk %>event/imgsprites/' },



            //shopserve
            png_top_sh: { expand: true, cwd: '<%= paths.devsh %>img/sprites/', src: '*.png', dest: '<%= paths.relsh %>img/sprites/' },
            png_info_sh: { expand: true, cwd: '<%= paths.devsh %>info/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>info/img/sprites/' },
            png_interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>common/interior/img/sprites/' },
            png_oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>common/oversea/img/sprites/' },
            png_common_sh: { expand: true, cwd: '<%= paths.devsh %>common/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>common/img/sprites/' },
            png_ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>common/ip/img/sprites/' },
            png_corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>corp/img/sprites/' },
            png_genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>genre/img/sprites/' },
            png_item_sh: { expand: true, cwd: '<%= paths.devsh %>item/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>item/img/sprites/' },
            png_blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>item/blogger/img/sprites/' },
            png_ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/img/sprites/', src: '*.png', dest: '<%= paths.relsh %>event/imgsprites/' },



            //yahoo
            png_top_ya: { expand: true, cwd: '<%= paths.devya %>img/sprites/', src: '*.png', dest: '<%= paths.relya %>img/sprites/' },
            png_info_ya: { expand: true, cwd: '<%= paths.devya %>info/img/sprites/', src: '*.png', dest: '<%= paths.relya %>info/img/sprites/' },
            png_interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/img/sprites/', src: '*.png', dest: '<%= paths.relya %>common/interior/img/sprites/' },
            png_oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/img/sprites/', src: '*.png', dest: '<%= paths.relya %>common/oversea/img/sprites/' },
            png_common_ya: { expand: true, cwd: '<%= paths.devya %>common/img/sprites/', src: '*.png', dest: '<%= paths.relya %>common/img/sprites/' },
            png_ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/img/sprites/', src: '*.png', dest: '<%= paths.relya %>common/ip/img/sprites/' },
            png_corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/img/sprites/', src: '*.png', dest: '<%= paths.relya %>corp/img/sprites/' },
            png_genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/img/sprites/', src: '*.png', dest: '<%= paths.relya %>genre/img/sprites/' },
            png_item_ya: { expand: true, cwd: '<%= paths.devya %>item/img/sprites/', src: '*.png', dest: '<%= paths.relya %>item/img/sprites/' },
            png_blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/img/sprites/', src: '*.png', dest: '<%= paths.relya %>item/blogger/img/sprites/' },
            png_ev_ya: { expand: true, cwd: '<%= paths.devya %>event/img/sprites/', src: '*.png', dest: '<%= paths.relya %>event/imgsprites/' }
        },


        //テスト環境のスプライト用png画像の削除
        clean: {
            png_rk: {
                src: [
                    '<%= paths.devrk %>*.png',
                    '<%= paths.devrk %>info/*.png',
                    '<%= paths.devrk %>common/interior/*.png',
                    '<%= paths.devrk %>common/oversea/*.png',
                    '<%= paths.devrk %>common/*.png',
                    '<%= paths.devrk %>corp/*.png',
                    '<%= paths.devrk %>genre/*.png',
                    '<%= paths.devrk %>item/*.png',
                    '<%= paths.devrk %>item/blogger/*.png',
                    '<%= paths.devrk %>event/*.png',
                ]
            },
            ipimages: {
                src: [
                    '<%= paths.relrk %>item/img/ip*.jpg',
                    '<%= paths.relrk %>item/img/sp*.jpg',
                    '<%= paths.relya %>item/img/ip*.jpg',
                    '<%= paths.relya %>item/img/sp*.jpg',
                    '<%= paths.relsh %>item/img/ip*.jpg'
                ]
            }
        },


        //csssprite
        //sprite: {
            /*top_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>img/*.png', destImg: '<%= paths.devrk %>img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>_scss/sprite.scss' },
            top_header_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>img/header/*.png', destImg: '<%= paths.devrk %>img/sprites/spritesheet_header.png', destCSS: '<%= paths.devrk %>_scss/sprite-header.scss' },
            top_footer_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>img/footer/*.png', destImg: '<%= paths.devrk %>img/sprites/spritesheet_footer.png', destCSS: '<%= paths.devrk %>_scss/sprite-footer.scss' },
            top_leftnav_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>img/leftnav/*.png', destImg: '<%= paths.devrk %>img/sprites/spritesheet_leftnav.png', destCSS: '<%= paths.devrk %>_scss/sprite-leftnav.scss' },

            frame_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>item/img/*.png', destImg: '<%= paths.devrk %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>item/_scss/sprite.scss' },
            info_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>info/img/*.png', destImg: '<%= paths.devrk %>info/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>info/_scss/sprite.scss' },
            interior_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>common/interior/img/*.png', destImg: '<%= paths.devrk %>common/interior/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>common/interior/_scss/sprite.scss' },
            oversea_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>common/oversea/img/*.png', destImg: '<%= paths.devrk %>common/oversea/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>common/oversea/_scss/sprite.scss' },
            common_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>common/img/*.png', destImg: '<%= paths.devrk %>common/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>common/_scss/sprite.scss' },
            ip_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>common/ip/img/*.png', destImg: '<%= paths.devrk %>common/ip/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>common/ip/_scss/sprite.scss' },
            corp_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>corp/img/*.png', destImg: '<%= paths.devrk %>corp/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>corp/_scss/sprite.scss' },
            genre_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>genre/img/*.png', destImg: '<%= paths.devrk %>genre/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>genre/_scss/sprite.scss' },
            item_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>item/img/*.png', destImg: '<%= paths.devrk %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>item/_scss/sprite.scss' },
            blog_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>item/blogger/img/*.png', destImg: '<%= paths.devrk %>item/blogger/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>item/blogger/_scss/sprite.scss' },
            ev_rk: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devrk %>event/img/*.png', destImg: '<%= paths.devrk %>event/img/sprites/spritesheet.png', destCSS: '<%= paths.devrk %>event/_scss/sprite.scss' },*/




            /*top_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>img/*.png', destImg: '<%= paths.devsh %>img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>_scss/sprite.scss' },
            top_sp_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>img/sp/*.png', destImg: '<%= paths.devsh %>img/sprites/spritesheet_sp.png', destCSS: '<%= paths.devsh %>_scss/sprite-sp.scss' },
            top_header_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>img/header/*.png', destImg: '<%= paths.devsh %>img/sprites/spritesheet_header.png', destCSS: '<%= paths.devsh %>_scss/sprite-header.scss' },
            top_footer_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>img/footer/*.png', destImg: '<%= paths.devsh %>img/sprites/spritesheet_footer.png', destCSS: '<%= paths.devsh %>_scss/sprite-footer.scss' },
            top_leftnav_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>img/leftnav/*.png', destImg: '<%= paths.devsh %>img/sprites/spritesheet_leftnav.png', destCSS: '<%= paths.devsh %>_scss/sprite-leftnav.scss' },

            frame_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>item/img/*.png', destImg: '<%= paths.devsh %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>item/_scss/sprite.scss' },
            info_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>info/img/*.png', destImg: '<%= paths.devsh %>info/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>info/_scss/sprite.scss' },
            interior_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>common/interior/img/*.png', destImg: '<%= paths.devsh %>common/interior/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>common/interior/_scss/sprite.scss' },
            oversea_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>common/oversea/img/*.png', destImg: '<%= paths.devsh %>common/oversea/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>common/oversea/_scss/sprite.scss' },
            common_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>common/img/*.png', destImg: '<%= paths.devsh %>common/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>common/_scss/sprite.scss' },
            ip_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>common/ip/img/*.png', destImg: '<%= paths.devsh %>common/ip/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>common/ip/_scss/sprite.scss' },
            corp_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>corp/img/*.png', destImg: '<%= paths.devsh %>corp/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>corp/_scss/sprite.scss' },
            genre_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>genre/img/*.png', destImg: '<%= paths.devsh %>genre/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>genre/_scss/sprite.scss' },
            item_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>item/img/*.png', destImg: '<%= paths.devsh %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>item/_scss/sprite.scss' },
            blog_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>item/blogger/img/*.png', destImg: '<%= paths.devsh %>item/blogger/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>item/blogger/_scss/sprite.scss' },
            ev_sh: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devsh %>event/img/*.png', destImg: '<%= paths.devsh %>event/img/sprites/spritesheet.png', destCSS: '<%= paths.devsh %>event/_scss/sprite.scss' },*/


            //yahoo
            /*top_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>img/*.png', destImg: '<%= paths.devya %>img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>_scss/sprite.scss' },
            top_header_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>img/header/*.png', destImg: '<%= paths.devya %>img/sprites/spritesheet_header.png', destCSS: '<%= paths.devya %>_scss/sprite-header.scss' },
            top_footer_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>img/footer/*.png', destImg: '<%= paths.devya %>img/sprites/spritesheet_footer.png', destCSS: '<%= paths.devya %>_scss/sprite-footer.scss' },
            top_leftnav_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>img/leftnav/*.png', destImg: '<%= paths.devya %>img/sprites/spritesheet_leftnav.png', destCSS: '<%= paths.devya %>_scss/sprite-leftnav.scss' },

            frame_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>item/img/*.png', destImg: '<%= paths.devya %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>item/_scss/sprite.scss' },
            info_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>info/img/*.png', destImg: '<%= paths.devya %>info/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>info/_scss/sprite.scss' },
            interior_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>common/interior/img/*.png', destImg: '<%= paths.devya %>common/interior/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>common/interior/_scss/sprite.scss' },
            oversea_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>common/oversea/img/*.png', destImg: '<%= paths.devya %>common/oversea/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>common/oversea/_scss/sprite.scss' },
            common_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>common/img/*.png', destImg: '<%= paths.devya %>common/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>common/_scss/sprite.scss' },
            ip_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>common/ip/img/*.png', destImg: '<%= paths.devya %>common/ip/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>common/ip/_scss/sprite.scss' },
            corp_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>corp/img/*.png', destImg: '<%= paths.devya %>corp/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>corp/_scss/sprite.scss' },
            genre_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>genre/img/*.png', destImg: '<%= paths.devya %>genre/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>genre/_scss/sprite.scss' },
            item_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>item/img/*.png', destImg: '<%= paths.devya %>item/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>item/_scss/sprite.scss' },
            blog_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>item/blogger/img/*.png', destImg: '<%= paths.devya %>item/blogger/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>item/blogger/_scss/sprite.scss' },
            ev_ya: { algorithm: 'binary-tree', padding: 10, cssFormat: 'scss', src: '<%= paths.devya %>event/img/*.png', destImg: '<%= paths.devya %>event/img/sprites/spritesheet.png', destCSS: '<%= paths.devya %>event/_scss/sprite.scss' }*/
        //},


        //htmlファイル縮小
        htmlmin: {
            options: {
                removeComments: true,
                removeCommentsFromCDATA: true,
                removeCDATASectionsFromCDATA: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                removeOptionalTags: true
            },
            //楽天用htmlファイルの縮小
            top_rk: { expand: true, cwd: '<%= paths.devrk %>', src: '*.html', dest: '<%= paths.relrk %>' },
            info_rk: { expand: true, cwd: '<%= paths.devrk %>info/', src: '*.html', dest: '<%= paths.relrk %>info/' },
            interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/', src: '*.html', dest: '<%= paths.relrk %>common/interior/' },
            oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/', src: '*.html', dest: '<%= paths.relrk %>common/oversea/' },
            common_rk: { expand: true, cwd: '<%= paths.devrk %>common/', src: '*.html', dest: '<%= paths.relrk %>common/' },
            ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/', src: '*.html', dest: '<%= paths.relrk %>common/ip/' },
            corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/', src: '*.html', dest: '<%= paths.relrk %>corp/' },
            genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/', src: '*.html', dest: '<%= paths.relrk %>genre/' },
            genre_month_rk: { expand: true, cwd: '<%= paths.devrk %>genre/month/', src: '*.html', dest: '<%= paths.relrk %>genre/month/' },
            item_rk: { expand: true, cwd: '<%= paths.devrk %>item/', src: '*.html', dest: '<%= paths.relrk %>item/' },
            blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/', src: '*.html', dest: '<%= paths.relrk %>item/blogger/' },
            ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/', src: '*.html', dest: '<%= paths.relrk %>event/' },



            //shopserve
            top_sh: { expand: true, cwd: '<%= paths.devsh %>', src: '*.html', dest: '<%= paths.relsh %>' },
            top_sh2: { expand: true, cwd: '<%= paths.devsh %>', src: '*.htm', dest: '<%= paths.relsh %>' },
            info_sh: { expand: true, cwd: '<%= paths.devsh %>info/', src: '*.html', dest: '<%= paths.relsh %>info/' },
            interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/', src: '*.html', dest: '<%= paths.relsh %>common/interior/' },
            oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/', src: '*.html', dest: '<%= paths.relsh %>common/oversea/' },
            common_sh: { expand: true, cwd: '<%= paths.devsh %>common/', src: '*.html', dest: '<%= paths.relsh %>common/' },
            ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/', src: '*.html', dest: '<%= paths.relsh %>common/ip/' },
            corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/', src: '*.html', dest: '<%= paths.relsh %>corp/' },
            genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/', src: '*.html', dest: '<%= paths.relsh %>genre/' },
            genre_month_sh: { expand: true, cwd: '<%= paths.devsh %>genre/month/', src: '*.html', dest: '<%= paths.relsh %>genre/month/' },
            item_sh: { expand: true, cwd: '<%= paths.devsh %>item/', src: '*.html', dest: '<%= paths.relsh %>item/' },
            blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/', src: '*.html', dest: '<%= paths.relsh %>item/blogger/' },
            ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/', src: '*.html', dest: '<%= paths.relsh %>event/' },



            //yahoo
            top_ya: { expand: true, cwd: '<%= paths.devya %>', src: '*.html', dest: '<%= paths.relya %>' },
            info_ya: { expand: true, cwd: '<%= paths.devya %>info/', src: '*.html', dest: '<%= paths.relya %>info/' },
            interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/', src: '*.html', dest: '<%= paths.relya %>common/interior/' },
            oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/', src: '*.html', dest: '<%= paths.relya %>common/oversea/' },
            common_ya: { expand: true, cwd: '<%= paths.devya %>common/', src: '*.html', dest: '<%= paths.relya %>common/' },
            ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/', src: '*.html', dest: '<%= paths.relya %>common/ip/' },
            corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/', src: '*.html', dest: '<%= paths.relya %>corp/' },
            genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/', src: '*.html', dest: '<%= paths.relya %>genre/' },
            genre_month_ya: { expand: true, cwd: '<%= paths.devya %>genre/month/', src: '*.html', dest: '<%= paths.relya %>genre/month/' },
            item_ya: { expand: true, cwd: '<%= paths.devya %>item/', src: '*.html', dest: '<%= paths.relya %>item/' },
            blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/', src: '*.html', dest: '<%= paths.relya %>item/blogger/' },
            ev_ya: { expand: true, cwd: '<%= paths.devya %>event/', src: '*.html', dest: '<%= paths.relya %>event/' }
        },

        //cssの整列
        csscomb: {
            frame_rk: { expand: true, cwd: '<%= paths.devrk %>frameworks/css/', src: '*.css', dest: '<%= paths.relrk %>frameworks/css/' },
            top_rk: { expand: true, cwd: '<%= paths.devrk %>css/', src: '*.css', dest: '<%= paths.relrk %>css/' },
            info_rk: { expand: true, cwd: '<%= paths.devrk %>info/css/', src: '*.css', dest: '<%= paths.relrk %>info/css/' },
            interior_rk: { expand: true, cwd: '<%= paths.devrk %>common/interior/css/', src: '*.css', dest: '<%= paths.relrk %>common/interior/css/' },
            oversea_rk: { expand: true, cwd: '<%= paths.devrk %>common/oversea/css/', src: '*.css', dest: '<%= paths.relrk %>common/oversea/css/' },
            common_rk: { expand: true, cwd: '<%= paths.devrk %>common/css/', src: '*.css', dest: '<%= paths.relrk %>common/css/' },
            ip_rk: { expand: true, cwd: '<%= paths.devrk %>common/ip/css/', src: '*.css', dest: '<%= paths.relrk %>common/ip/css/' },
            corp_rk: { expand: true, cwd: '<%= paths.devrk %>corp/css/', src: '*.css', dest: '<%= paths.relrk %>corp/css/' },
            genre_rk: { expand: true, cwd: '<%= paths.devrk %>genre/css/', src: '*.css', dest: '<%= paths.relrk %>genre/css/' },
            item_rk: { expand: true, cwd: '<%= paths.devrk %>item/css/', src: '*.css', dest: '<%= paths.relrk %>item/css/' },
            blog_rk: { expand: true, cwd: '<%= paths.devrk %>item/blogger/css/', src: '*.css', dest: '<%= paths.relrk %>item/blogger/css/' },
            ev_rk: { expand: true, cwd: '<%= paths.devrk %>event/css/', src: '*.css', dest: '<%= paths.relrk %>event/css/' },



            //shopserve
            frame_sh: { expand: true, cwd: '<%= paths.devsh %>frameworks/css/', src: '*.css', dest: '<%= paths.relsh %>frameworks/css/' },
            top_sh: { expand: true, cwd: '<%= paths.devsh %>css/', src: '*.css', dest: '<%= paths.relsh %>css/' },
            info_sh: { expand: true, cwd: '<%= paths.devsh %>info/css/', src: '*.css', dest: '<%= paths.relsh %>info/css/' },
            interior_sh: { expand: true, cwd: '<%= paths.devsh %>common/interior/css/', src: '*.css', dest: '<%= paths.relsh %>common/interior/css/' },
            oversea_sh: { expand: true, cwd: '<%= paths.devsh %>common/oversea/css/', src: '*.css', dest: '<%= paths.relsh %>common/oversea/css/' },
            common_sh: { expand: true, cwd: '<%= paths.devsh %>common/css/', src: '*.css', dest: '<%= paths.relsh %>common/css/' },
            ip_sh: { expand: true, cwd: '<%= paths.devsh %>common/ip/css/', src: '*.css', dest: '<%= paths.relsh %>common/ip/css/' },
            corp_sh: { expand: true, cwd: '<%= paths.devsh %>corp/css/', src: '*.css', dest: '<%= paths.relsh %>corp/css/' },
            genre_sh: { expand: true, cwd: '<%= paths.devsh %>genre/css/', src: '*.css', dest: '<%= paths.relsh %>genre/css/' },
            item_sh: { expand: true, cwd: '<%= paths.devsh %>item/css/', src: '*.css', dest: '<%= paths.relsh %>item/css/' },
            blog_sh: { expand: true, cwd: '<%= paths.devsh %>item/blogger/css/', src: '*.css', dest: '<%= paths.relsh %>item/blogger/css/' },
            ev_sh: { expand: true, cwd: '<%= paths.devsh %>event/css/', src: '*.css', dest: '<%= paths.relsh %>event/css/' },



            //yahoo
            frame_ya: { expand: true, cwd: '<%= paths.devya %>frameworks/css/', src: '*.css', dest: '<%= paths.relya %>frameworks/css/' },
            top_ya: { expand: true, cwd: '<%= paths.devya %>css/', src: '*.css', dest: '<%= paths.relya %>css/' },
            info_ya: { expand: true, cwd: '<%= paths.devya %>info/css/', src: '*.css', dest: '<%= paths.relya %>info/css/' },
            interior_ya: { expand: true, cwd: '<%= paths.devya %>common/interior/css/', src: '*.css', dest: '<%= paths.relya %>common/interior/css/' },
            oversea_ya: { expand: true, cwd: '<%= paths.devya %>common/oversea/css/', src: '*.css', dest: '<%= paths.relya %>common/oversea/css/' },
            common_ya: { expand: true, cwd: '<%= paths.devya %>common/css/', src: '*.css', dest: '<%= paths.relya %>common/css/' },
            ip_ya: { expand: true, cwd: '<%= paths.devya %>common/ip/css/', src: '*.css', dest: '<%= paths.relya %>common/ip/css/' },
            corp_ya: { expand: true, cwd: '<%= paths.devya %>corp/css/', src: '*.css', dest: '<%= paths.relya %>corp/css/' },
            genre_ya: { expand: true, cwd: '<%= paths.devya %>genre/css/', src: '*.css', dest: '<%= paths.relya %>genre/css/' },
            item_ya: { expand: true, cwd: '<%= paths.devya %>item/css/', src: '*.css', dest: '<%= paths.relya %>item/css/' },
            blog_ya: { expand: true, cwd: '<%= paths.devya %>item/blogger/css/', src: '*.css', dest: '<%= paths.relya %>item/blogger/css/' },
            ev_ya: { expand: true, cwd: '<%= paths.devya %>event/css/', src: '*.css', dest: '<%= paths.relya %>event/css/' },

            //全店舗共有
            info_cmn: { expand: true, cwd: '<%= paths.devcmn %>info/css/', src: '*.css', dest: '<%= paths.relcmn %>info/css/' },
            cmn_cmn: { expand: true, cwd: '<%= paths.devcmn %>common/css/', src: '*.css', dest: '<%= paths.relcmn %>common/css/' }

        },

        //jsの圧縮
        uglify: {
            frame_rk: { expand: true, cwd: '<%= paths.relrk %>frameworks/js/', src: '*.js', dest: '<%= paths.relrk %>frameworks/js/' },
            top_rk: { expand: true, cwd: '<%= paths.relrk %>js/', src: '*.js', dest: '<%= paths.relrk %>js/' },
            info_rk: { expand: true, cwd: '<%= paths.relrk %>info/js/', src: '*.js', dest: '<%= paths.relrk %>info/js/' },
            interior_rk: { expand: true, cwd: '<%= paths.relrk %>common/interior/js/', src: '*.js', dest: '<%= paths.relrk %>common/interior/js/' },
            oversea_rk: { expand: true, cwd: '<%= paths.relrk %>common/oversea/js/', src: '*.js', dest: '<%= paths.relrk %>common/oversea/js/' },
            common_rk: { expand: true, cwd: '<%= paths.relrk %>common/js/', src: '*.js', dest: '<%= paths.relrk %>common/js/' },
            ip_rk: { expand: true, cwd: '<%= paths.relrk %>common/ip/js/', src: '*.js', dest: '<%= paths.relrk %>common/ip/js/' },
            corp_rk: { expand: true, cwd: '<%= paths.relrk %>corp/js/', src: '*.js', dest: '<%= paths.relrk %>corp/js/' },
            genre_rk: { expand: true, cwd: '<%= paths.relrk %>genre/js/', src: '*.js', dest: '<%= paths.relrk %>genre/js/' },
            item_rk: { expand: true, cwd: '<%= paths.relrk %>item/js/', src: '*.js', dest: '<%= paths.relrk %>item/js/' },
            blog_rk: { expand: true, cwd: '<%= paths.relrk %>item/blogger/js/', src: '*.js', dest: '<%= paths.relrk %>item/blogger/js/' },
            ev_rk: { expand: true, cwd: '<%= paths.relrk %>event/js/', src: '*.js', dest: '<%= paths.relrk %>event/js/' },



            //shopserve
            frame_sh: { expand: true, cwd: '<%= paths.relsh %>frameworks/js/', src: '*.js', dest: '<%= paths.relsh %>frameworks/js/' },
            top_sh: { expand: true, cwd: '<%= paths.relsh %>js/', src: '*.js', dest: '<%= paths.relsh %>js/' },
            info_sh: { expand: true, cwd: '<%= paths.relsh %>info/js/', src: '*.js', dest: '<%= paths.relsh %>info/js/' },
            interior_sh: { expand: true, cwd: '<%= paths.relsh %>common/interior/js/', src: '*.js', dest: '<%= paths.relsh %>common/interior/js/' },
            oversea_sh: { expand: true, cwd: '<%= paths.relsh %>common/oversea/js/', src: '*.js', dest: '<%= paths.relsh %>common/oversea/js/' },
            common_sh: { expand: true, cwd: '<%= paths.relsh %>common/js/', src: '*.js', dest: '<%= paths.relsh %>common/js/' },
            ip_sh: { expand: true, cwd: '<%= paths.relsh %>common/ip/js/', src: '*.js', dest: '<%= paths.relsh %>common/ip/js/' },
            corp_sh: { expand: true, cwd: '<%= paths.relsh %>corp/js/', src: '*.js', dest: '<%= paths.relsh %>corp/js/' },
            genre_sh: { expand: true, cwd: '<%= paths.relsh %>genre/js/', src: '*.js', dest: '<%= paths.relsh %>genre/js/' },
            item_sh: { expand: true, cwd: '<%= paths.relsh %>item/js/', src: '*.js', dest: '<%= paths.relsh %>item/js/' },
            blog_sh: { expand: true, cwd: '<%= paths.relsh %>item/blogger/js/', src: '*.js', dest: '<%= paths.relsh %>item/blogger/js/' },
            ev_sh: { expand: true, cwd: '<%= paths.relsh %>event/js/', src: '*.js', dest: '<%= paths.relsh %>event/js/' },



            //yahoo
            frame_ya: { expand: true, cwd: '<%= paths.relya %>frameworks/js/', src: '*.js', dest: '<%= paths.relya %>frameworks/js/' },
            top_ya: { expand: true, cwd: '<%= paths.relya %>js/', src: '*.js', dest: '<%= paths.relya %>js/' },
            info_ya: { expand: true, cwd: '<%= paths.relya %>info/js/', src: '*.js', dest: '<%= paths.relya %>info/js/' },
            interior_ya: { expand: true, cwd: '<%= paths.relya %>common/interior/js/', src: '*.js', dest: '<%= paths.relya %>common/interior/js/' },
            oversea_ya: { expand: true, cwd: '<%= paths.relya %>common/oversea/js/', src: '*.js', dest: '<%= paths.relya %>common/oversea/js/' },
            common_ya: { expand: true, cwd: '<%= paths.relya %>common/js/', src: '*.js', dest: '<%= paths.relya %>common/js/' },
            ip_ya: { expand: true, cwd: '<%= paths.relya %>common/ip/js/', src: '*.js', dest: '<%= paths.relya %>common/ip/js/' },
            corp_ya: { expand: true, cwd: '<%= paths.relya %>corp/js/', src: '*.js', dest: '<%= paths.relya %>corp/js/' },
            genre_ya: { expand: true, cwd: '<%= paths.relya %>genre/js/', src: '*.js', dest: '<%= paths.relya %>genre/js/' },
            item_ya: { expand: true, cwd: '<%= paths.relya %>item/js/', src: '*.js', dest: '<%= paths.relya %>item/js/' },
            blog_ya: { expand: true, cwd: '<%= paths.relya %>item/blogger/js/', src: '*.js', dest: '<%= paths.relya %>item/blogger/js/' },
            ev_ya: { expand: true, cwd: '<%= paths.relya %>event/js/', src: '*.js', dest: '<%= paths.relya %>event/js/' }

        },


        //ファイルの結合
        /*concat: {
            options: {
                separator: ';'
            },
            frame_rk: { src: '<%= paths.devrk %>frameworks/js/*.js', dest: '<%= paths.devrk %>frameworks/js/con-frameworks.js' },
            top_rk: { src: '<%= paths.devrk %>js/*.js', dest: '<%= paths.devrk %>js/con-top.js' },
            info_rk: { src: '<%= paths.devrk %>info/js/*.js', dest: '<%= paths.devrk %>info/js/con-info.js' },
            interior_rk: { src: '<%= paths.devrk %>common/interior/js/*.js', dest: '<%= paths.devrk %>common/interior/js/con-interior.js' },
            oversea_rk: { src: '<%= paths.devrk %>common/oversea/js/*.js', dest: '<%= paths.devrk %>common/oversea/js/con-oversea.js' },
            common_rk: { src: '<%= paths.devrk %>common/js/*.js', dest: '<%= paths.devrk %>common/js/con-common.js' },
            ip_rk: { src: '<%= paths.devrk %>common/ip/js/*.js', dest: '<%= paths.devrk %>common/ip/js/con-oversea.js' },
            corp_rk: { src: '<%= paths.devrk %>corp/js/*.js', dest: '<%= paths.devrk %>corp/js/con-corp.js' },
            genre_rk: { src: '<%= paths.devrk %>genre/js/*.js', dest: '<%= paths.devrk %>genre/js/con-genre.js' },
            item_rk: { src: '<%= paths.devrk %>item/js/*.js', dest: '<%= paths.devrk %>item/js/con-item.js' },
            blog_rk: { src: '<%= paths.devrk %>item/blogger/js/*.js', dest: '<%= paths.devrk %>item/blogger/js/con-blogger.js' },
            ev_rk: { src: '<%= paths.devrk %>event/js/*.js', dest: '<%= paths.devrk %>event/js/con-event.js' },



            //shopserve
            frame_sh: { src: '<%= paths.devsh %>frameworks/js/*.js', dest: '<%= paths.devsh %>frameworks/js/con-frameworks.js' },
            top_sh: { src: '<%= paths.devsh %>js/*.js', dest: '<%= paths.devsh %>js/con-top.js' },
            info_sh: { src: '<%= paths.devsh %>info/js/*.js', dest: '<%= paths.devsh %>info/js/con-info.js' },
            interior_sh: { src: '<%= paths.devsh %>common/interior/js/*.js', dest: '<%= paths.devsh %>common/interior/js/con-interior.js' },
            oversea_sh: { src: '<%= paths.devsh %>common/oversea/js/*.js', dest: '<%= paths.devsh %>common/oversea/js/con-oversea.js' },
            common_sh: { src: '<%= paths.devsh %>common/js/*.js', dest: '<%= paths.devsh %>common/js/con-common.js' },
            ip_sh: { src: '<%= paths.devsh %>common/ip/js/*.js', dest: '<%= paths.devsh %>common/ip/js/con-oversea.js' },
            corp_sh: { src: '<%= paths.devsh %>corp/js/*.js', dest: '<%= paths.devsh %>corp/js/con-corp.js' },
            genre_sh: { src: '<%= paths.devsh %>genre/js/*.js', dest: '<%= paths.devsh %>genre/js/con-genre.js' },
            item_sh: { src: '<%= paths.devsh %>item/js/*.js', dest: '<%= paths.devsh %>item/js/con-item.js' },
            blog_sh: { src: '<%= paths.devsh %>item/blogger/js/*.js', dest: '<%= paths.devsh %>item/blogger/js/con-blogger.js' },
            ev_sh: { src: '<%= paths.devsh %>event/js/*.js', dest: '<%= paths.devsh %>event/js/con-event.js' },



            //yahoo
            frame_ya: { src: '<%= paths.devya %>frameworks/js/*.js', dest: '<%= paths.devya %>frameworks/js/con-frameworks.js' },
            top_ya: { src: '<%= paths.devya %>js/*.js', dest: '<%= paths.devya %>js/con-top.js' },
            info_ya: { src: '<%= paths.devya %>info/js/*.js', dest: '<%= paths.devya %>info/js/con-info.js' },
            interior_ya: { src: '<%= paths.devya %>common/interior/js/*.js', dest: '<%= paths.devya %>common/interior/js/con-interior.js' },
            oversea_ya: { src: '<%= paths.devya %>common/oversea/js/*.js', dest: '<%= paths.devya %>common/oversea/js/con-oversea.js' },
            common_ya: { src: '<%= paths.devya %>common/js/*.js', dest: '<%= paths.devya %>common/js/con-common.js' },
            ip_ya: { src: '<%= paths.devya %>common/ip/js/*.js', dest: '<%= paths.devya %>common/ip/js/con-oversea.js' },
            corp_ya: { src: '<%= paths.devya %>corp/js/*.js', dest: '<%= paths.devya %>corp/js/con-corp.js' },
            genre_ya: { src: '<%= paths.devya %>genre/js/*.js', dest: '<%= paths.devya %>genre/js/con-genre.js' },
            item_ya: { src: '<%= paths.devya %>item/js/*.js', dest: '<%= paths.devya %>item/js/con-item.js' },
            blog_ya: { src: '<%= paths.devya %>item/blogger/js/*.js', dest: '<%= paths.devya %>item/blogger/js/con-blogger.js' },
            ev_ya: { src: '<%= paths.devya %>event/js/*.js', dest: '<%= paths.devya %>event/js/con-event.js' },
        },*/

        //自動リロード設定
        connect: {
            server: {
                options: {
                    port: 9000,
                    base: 'html'
                }
            }
        },

        styleguide: {
            dist: {
                name: 'Style Guide',
                options: {
                    framework: {
                        name: 'styledocco'
                    },
                },
                files: {
                    '<%= paths.devrk %>styleguide': '<%= paths.devrk %>styleguide/css/guide.css',
                    '<%= paths.devsh %>styleguide': '<%= paths.devsh %>styleguide/css/guide.css'
                }
            }
        },

        //商品画像自動アップロード
        'ftp-deploy': {
            build_rk: {
                auth: {
                    host: 'ftp.rakuten.ne.jp',
                    port: 16910,
                    authKey: 'key1'
                },
                src: 'rel/rakuten/item/img/',
                dest: 'item/img/',
                exclusions: ['**/.DS_Store', '**/.BridgeSort'],
            },
            build_ya: {
                auth: {
                    host: 'ftp.geocities.jp',
                    port: 21,
                    authKey: 'key2'
                },
                src: 'rel/yahoo/item/img/',
                dest: 'item/img/',
                exclusions: ['**/.DS_Store', '**/.BridgeSort']
            },
            build_sh: {
                auth: {
                    host: 'ftp.shopserve.jp',
                    port: 21,
                    authKey: 'key3'
                },
                src: 'rel/shopserve/item/img/',
                dest: 'docs/item/img/',
                exclusions: ['**/.DS_Store', '**/.BridgeSort']
            }
        },

        // Watch
        watch: {
            // options
            options: {
                spawn: false,
                livereload: true,
                event: ['added', 'changed']
            },

            guide_rk: { files: ['<%= paths.devrk %>styleguide/_scss/*.scss'], tasks: ['compass:guide_rk', 'styleguide'] },
            guide_sh: { files: ['<%= paths.devsh %>styleguide/_scss/*.scss'], tasks: ['compass:guide_sh', 'styleguide'] },
            guide_ya: { files: ['<%= paths.devya %>styleguide/_scss/*.scss'], tasks: ['compass:guide_ya', 'styleguide'] },

            // 楽天商品ページの監視
            html_top_rk: { files: ['<%= paths.devrk %>*.html'], tasks: ['htmlmin:top_rk'] },
            html_info_rk: { files: ['<%= paths.devrk %>info/*.html'], tasks: ['htmlmin:info_rk'] },
            html_interior_rk: { files: ['<%= paths.devrk %>common/interior/*.html'], tasks: ['htmlmin:interior_rk'] },
            html_oversea_rk: { files: ['<%= paths.devrk %>common/oversea/*.html'], tasks: ['htmlmin:oversea_rk'] },
            html_common_rk: { files: ['<%= paths.devrk %>common/*.html'], tasks: ['htmlmin:common_rk'] },
            html_ip_rk: { files: ['<%= paths.devrk %>common/ip/*.html'], tasks: ['htmlmin:ip_rk'] },
            html_corp_rk: { files: ['<%= paths.devrk %>corp/*.html'], tasks: ['htmlmin:corp_rk'] },
            html_genre_rk: { files: ['<%= paths.devrk %>genre/*.html'], tasks: ['htmlmin:genre_rk'] },
            html_genre_month_rk: { files: ['<%= paths.devrk %>genre/month/*.html'], tasks: ['htmlmin:genre_month_rk'] },
            html_item_rk: { files: ['<%= paths.devrk %>item/*.html'], tasks: ['htmlmin:item_rk'] },
            html_blog_rk: { files: ['<%= paths.devrk %>item/blogger/*.html'], tasks: ['htmlmin:blog_rk'] },
            html_ev_rk: { files: ['<%= paths.devrk %>event/*.html'], tasks: ['htmlmin:ev_rk'] },

            css_frame_rk: { files: ['<%= paths.devrk %>frameworks/_scss/*.scss'], tasks: ['compass:frame_rk', 'csscomb:frame_rk', 'copy:css_frame_rk', 'compress:css_frame_rk'] },
            css_top_rk: { files: ['<%= paths.devrk %>_scss/*.scss'], tasks: ['compass:top_rk', 'csscomb:top_rk', 'copy:css_top_rk', 'compress:css_top_rk'] },
            css_info_rk: { files: ['<%= paths.devrk %>info/_scss/*.scss'], tasks: ['compass:info_rk', 'csscomb:info_rk', 'copy:css_info_rk', 'compress:css_info_rk'] },
            css_interior_rk: { files: ['<%= paths.devrk %>common/interior/_scss/*.scss'], tasks: ['compass:interior_rk', 'csscomb:interior_rk', 'copy:css_interior_rk', 'compress:css_interior_rk'] },
            css_oversea_rk: { files: ['<%= paths.devrk %>common/oversea/_scss/*.scss'], tasks: ['compass:oversea_rk', 'csscomb:oversea_rk', 'copy:css_oversea_rk', 'compress:css_oversea_rk'] },
            css_common_rk: { files: ['<%= paths.devrk %>common/_scss/*.scss'], tasks: ['compass:common_rk', 'csscomb:common_rk', 'copy:css_common_rk', 'compress:css_common_rk'] },
            css_ip_rk: { files: ['<%= paths.devrk %>common/ip/_scss/*.scss'], tasks: ['compass:ip_rk', 'csscomb:ip_rk', 'copy:css_ip_rk', 'compress:css_ip_rk'] },
            css_corp_rk: { files: ['<%= paths.devrk %>corp/_scss/*.scss'], tasks: ['compass:corp_rk', 'csscomb:corp_rk', 'copy:css_corp_rk', 'compress:css_corp_rk'] },
            css_genre_rk: { files: ['<%= paths.devrk %>genre/_scss/*.scss'], tasks: ['compass:genre_rk', 'csscomb:genre_rk', 'copy:css_genre_rk', 'compress:css_genre_rk'] },
            css_item_rk: { files: ['<%= paths.devrk %>item/_scss/*.scss'], tasks: ['compass:item_rk', 'csscomb:item_rk', 'copy:css_item_rk', 'compress:css_item_rk'] },
            css_blog_rk: { files: ['<%= paths.devrk %>item/blogger/_scss/*.scss'], tasks: ['compass:blog_rk', 'csscomb:blog_rk', 'copy:css_blog_rk', 'compress:css_blog_rk'] },
            css_ev_rk: { files: ['<%= paths.devrk %>event/_scss/*.scss'], tasks: ['compass:ev_rk', 'csscomb:ev_rk', 'copy:css_ev_rk', 'compress:css_ev_rk'] },

            /*csss_frame_rk:{files:['<%= paths.devrk %>frameworks/css/*.css'],tasks:['csscomb:frame_rk','copy:css_frame_rk','compress:css_frame_rk']},
            csss_top_rk:{files:['<%= paths.devrk %>css/*.css'],tasks:['csscomb:top_rk','copy:css_top_rk','compress:css_top_rk']},
            csss_info_rk:{files:['<%= paths.devrk %>info/css/*.css'],tasks:['csscomb:info_rk','copy:css_info_rk','compress:css_info_rk']},
            csss_interior_rk:{files:['<%= paths.devrk %>common/interior/css/*.css'],tasks:['csscomb:interior_rk','copy:css_interior_rk','compress:css_interior_rk']},
            csss_oversea_rk:{files:['<%= paths.devrk %>common/oversea/css/*.css'],tasks:['csscomb:oversea_rk','copy:css_oversea_rk','compress:css_oversea_rk']},
            csss_corp_rk:{files:['<%= paths.devrk %>corp/css/*.css'],tasks:['csscomb:corp_rk','copy:css_corp_rk','compress:css_corp_rk']},
            csss_genre_rk:{files:['<%= paths.devrk %>genre/css/*.css'],tasks:['csscomb:genre_rk','copy:css_genre_rk','compress:css_genre_rk']},
            csss_item_rk:{files:['<%= paths.devrk %>item/css/*.css'],tasks:['csscomb:item_rk','copy:css_item_rk','compress:css_item_rk']},
            csss_blog_rk:{files:['<%= paths.devrk %>item/blogger/css/*.css'],tasks:['csscomb:blog_rk','copy:css_blog_rk','compress:css_blog_rk']},
            csss_ev_rk:{files:['<%= paths.devrk %>event/css/*.css'],tasks:['csscomb:ev_rk','copy:css_ev_rk','compress:css_ev_rk']},*/

            js_frame_rk: { files: ['<%= paths.devrk %>frameworks/js/*.js'], tasks: ['copy:js_frame_rk', 'uglify:frame_rk', 'compress:js_frame_rk'] },
            js_top_rk: { files: ['<%= paths.devrk %>js/*.js'], tasks: ['copy:js_top_rk', 'uglify:top_rk', 'compress:js_top_rk'] },
            js_info_rk: { files: ['<%= paths.devrk %>info/js/*.js'], tasks: ['copy:js_info_rk', 'uglify:info_rk', 'compress:js_info_rk'] },
            js_interior_rk: { files: ['<%= paths.devrk %>common/interior/js/*.js'], tasks: ['copy:js_interior_rk', 'uglify:interior_rk', 'compress:js_interior_rk'] },
            js_oversea_rk: { files: ['<%= paths.devrk %>common/oversea/js/*.js'], tasks: ['copy:js_oversea_rk', 'uglify:oversea_rk', 'compress:js_oversea_rk'] },
            js_common_rk: { files: ['<%= paths.devrk %>common/js/*.js'], tasks: ['copy:js_common_rk', 'uglify:common_rk', 'compress:js_common_rk'] },
            js_ip_rk: { files: ['<%= paths.devrk %>common/ip/js/*.js'], tasks: ['copy:js_ip_rk', 'uglify:ip_rk', 'compress:js_ip_rk'] },
            js_corp_rk: { files: ['<%= paths.devrk %>corp/js/*.js'], tasks: ['copy:js_corp_rk', 'uglify:corp_rk', 'compress:js_corp_rk'] },
            js_genre_rk: { files: ['<%= paths.devrk %>genre/js/*.js'], tasks: ['copy:js_genre_rk', 'uglify:genre_rk', 'compress:js_genre_rk'] },
            js_item_rk: { files: ['<%= paths.devrk %>item/js/*.js'], tasks: ['copy:js_item_rk', 'uglify:item_rk', 'compress:js_item_rk'] },
            js_blog_rk: { files: ['<%= paths.devrk %>item/blogger/js/*.js'], tasks: ['copy:js_blog_rk', 'uglify:blog_rk', 'compress:js_blog_rk'] },
            js_ev_rk: { files: ['<%= paths.devrk %>event/js/*.js'], tasks: ['copy:js_ev_rk', 'uglify:ev_rk', 'compress:js_ev_rk'] },

            //pngスプライト画像生成、最適化
            /*png_top_rk: { files: ['<%= paths.devrk %>img/*.png'], tasks: ['sprite:top_rk', 'imageoptim:png_top_rk', 'copy:png_top_rk'] },
            png_top_header_rk: { files: ['<%= paths.devrk %>img/header/*.png'], tasks: ['sprite:top_header_rk', 'imageoptim:png_top_rk', 'copy:png_top_rk'] },
            png_top_footer_rk: { files: ['<%= paths.devrk %>img/footer/*.png'], tasks: ['sprite:top_footer_rk', 'imageoptim:png_top_rk', 'copy:png_top_rk'] },
            png_top_leftnav_rk: { files: ['<%= paths.devrk %>img/leftnav/*.png'], tasks: ['sprite:top_leftnav_rk', 'imageoptim:png_top_rk', 'copy:png_top_rk'] },

            png_info_rk: { files: ['<%= paths.devrk %>info/img/*.png'], tasks: ['sprite:info_rk', 'imageoptim:png_info_rk', 'copy:png_info_rk'] },
            png_interior_rk: { files: ['<%= paths.devrk %>common/interior/img/*.png'], tasks: ['sprite:interior_rk', 'imageoptim:png_interior_rk', 'copy:png_interior_rk'] },
            png_oversea_rk: { files: ['<%= paths.devrk %>common/oversea/img/*.png'], tasks: ['sprite:oversea_rk', 'imageoptim:png_oversea_rk', 'copy:png_oversea_rk'] },
            png_common_rk: { files: ['<%= paths.devrk %>common/img/*.png'], tasks: ['sprite:common_rk', 'imageoptim:png_common_rk', 'copy:png_common_rk'] },
            png_ip_rk: { files: ['<%= paths.devrk %>common/ip/img/*.png'], tasks: ['sprite:ip_rk', 'imageoptim:png_ip_rk', 'copy:png_ip_rk'] },
            png_corp_rk: { files: ['<%= paths.devrk %>corp/img/*.png'], tasks: ['sprite:corp_rk', 'imageoptim:png_corp_rk', 'copy:png_corp_rk'] },
            png_genre_rk: { files: ['<%= paths.devrk %>genre/img/*.png'], tasks: ['sprite:genre_rk', 'imageoptim:png_genre_rk', 'copy:png_genre_rk'] },
            png_item_rk: { files: ['<%= paths.devrk %>item/img/*.png'], tasks: ['sprite:item_rk', 'imageoptim:png_item_rk', 'copy:png_item_rk'] },
            png_blog_rk: { files: ['<%= paths.devrk %>item/blogger/img/*.png'], tasks: ['sprite:blog_rk', 'imageoptim:png_blog_rk', 'copy:png_blog_rk'] },
            png_ev_rk: { files: ['<%= paths.devrk %>event/img/*.png'], tasks: ['sprite:ev_rk', 'imageoptim:png_ev_rk', 'copy:png_ev_rk'] },*/

            //jpg画像最適化
            jpg_top_rk: { files: ['<%= paths.devrk %>img/*.jpg'], tasks: ['imageoptim:jpg_top_rk', 'copy:jpg_top_rk'] },
            jpg_top_header_rk: { files: ['<%= paths.devrk %>img/header/*.jpg'], tasks: ['imageoptim:jpg_top_header_rk', 'copy:jpg_top_header_rk'] },
            jpg_top_footer_rk: { files: ['<%= paths.devrk %>img/footer/*.jpg'], tasks: ['imageoptim:jpg_top_footer_rk', 'copy:jpg_top_footer_rk'] },
            jpg_top_leftnav_rk: { files: ['<%= paths.devrk %>img/leftnav/*.jpg'], tasks: ['imageoptim:jpg_top_leftnav_rk', 'copy:jpg_top_leftnav_rk'] },
            //楽天の商品画像をYahooに複製、アップロードした後に削除
            jpg_ipcopy: { files: ['<%= paths.relrk %>item/img/*.jpg'], tasks: ['copy:jpg_ipcopy_rk_sp', 'copy:jpg_ipcopy_rk_ip_ya', 'copy:jpg_ipcopy_rk_ip_sh'] },
            jpg_ipdeploy: { files: ['<%= paths.relrk %>item/img/*.jpg'], tasks: ['ftp-deploy:build_ya', 'ftp-deploy:build_sh', 'ftp-deploy:build_rk'] },
            /*jpg_itemdeploy_rk: { files: ['<%= paths.relrk %>item/css/ip-style.css'], tasks: ['ftp-deploy:update_item_rk'] },
            jpg_itemdeploy_ya: { files: ['<%= paths.relya %>item/css/ip-style.css'], tasks: ['ftp-deploy:update_item_ya'] },
            jpg_itemdeploy_sh: { files: ['<%= paths.relsh %>css/style-ip.css'], tasks: ['ftp-deploy:update_item_sh'] },*/
            jpg_ipcopy_rk_dt: { files: ['<%= paths.relrk %>item/img/ip*.jpg'], tasks: ['copy:jpg_ipcopy_dt_ip'] },
            jpg_ipcopy_rk_dt: { files: ['<%= paths.relrk %>item/img/sp*.jpg'], tasks: ['copy:jpg_ipcopy_dt_sp'] },

            jpg_info_rk: { files: ['<%= paths.devrk %>info/img/*.jpg'], tasks: ['imageoptim:jpg_info_rk', 'copy:jpg_info_rk'] },
            jpg_interior_rk: { files: ['<%= paths.devrk %>common/interior/img/*.jpg'], tasks: ['imageoptim:jpg_interior_rk', 'copy:jpg_interior_rk'] },
            jpg_oversea_rk: { files: ['<%= paths.devrk %>common/oversea/img/*.jpg'], tasks: ['imageoptim:jpg_oversea_rk', 'copy:jpg_oversea_rk'] },
            jpg_common_rk: { files: ['<%= paths.devrk %>common/img/*.jpg'], tasks: ['imageoptim:jpg_common_rk', 'copy:jpg_common_rk'] },
            jpg_ip_rk: { files: ['<%= paths.devrk %>common/ip/img/*.jpg'], tasks: ['imageoptim:jpg_ip_rk', 'copy:jpg_ip_rk'] },
            jpg_corp_rk: { files: ['<%= paths.devrk %>corp/img/*.jpg'], tasks: ['imageoptim:jpg_corp_rk', 'copy:jpg_corp_rk'] },
            jpg_genre_rk: { files: ['<%= paths.devrk %>genre/img/*.jpg'], tasks: ['imageoptim:jpg_genre_rk', 'copy:jpg_genre_rk'] },
            jpg_genre_month_rk: { files: ['<%= paths.devrk %>genre/month/img/*.jpg'], tasks: ['imageoptim:jpg_genre_month_rk', 'copy:jpg_genre_month_rk'] },
            jpg_item_rk: { files: ['<%= paths.devrk %>item/img/*.jpg'], tasks: ['imageoptim:jpg_item_rk', 'copy:jpg_item_rk'] },
            jpg_blog_rk: { files: ['<%= paths.devrk %>item/blogger/img/*.jpg'], tasks: ['imageoptim:jpg_blog_rk', 'copy:jpg_blog_rk'] },
            jpg_ev_rk: { files: ['<%= paths.devrk %>event/img/*.jpg'], tasks: ['imageoptim:jpg_ev_rk', 'copy:jpg_ev_rk'] },







            // shopserve 監視
            html_top_sh: { files: ['<%= paths.devsh %>*.html'], tasks: ['htmlmin:top_sh'] },
            html_top_sh2: { files: ['<%= paths.devsh %>*.htm'], tasks: ['htmlmin:top_sh2'] },
            html_info_sh: { files: ['<%= paths.devsh %>info/*.html'], tasks: ['htmlmin:info_sh'] },
            html_interior_sh: { files: ['<%= paths.devsh %>common/interior/*.html'], tasks: ['htmlmin:interior_sh'] },
            html_oversea_sh: { files: ['<%= paths.devsh %>common/oversea/*.html'], tasks: ['htmlmin:oversea_sh'] },
            html_common_sh: { files: ['<%= paths.devsh %>common/*.html'], tasks: ['htmlmin:common_sh'] },
            html_ip_sh: { files: ['<%= paths.devsh %>common/ip/*.html'], tasks: ['htmlmin:ip_sh'] },
            html_corp_sh: { files: ['<%= paths.devsh %>corp/*.html'], tasks: ['htmlmin:corp_sh'] },
            html_genre_sh: { files: ['<%= paths.devsh %>genre/*.html'], tasks: ['htmlmin:genre_sh'] },
            html_genre_month_sh: { files: ['<%= paths.devsh %>genre/month/*.html'], tasks: ['htmlmin:genre_month_sh'] },
            html_item_sh: { files: ['<%= paths.devsh %>item/*.html'], tasks: ['htmlmin:item_sh'] },
            html_blog_sh: { files: ['<%= paths.devsh %>item/blogger/*.html'], tasks: ['htmlmin:blog_sh'] },
            html_ev_sh: { files: ['<%= paths.devsh %>event/*.html'], tasks: ['htmlmin:ev_sh'] },

            css_frame_sh: { files: ['<%= paths.devsh %>frameworks/_scss/*.scss'], tasks: ['compass:frame_sh', 'csscomb:frame_sh', 'copy:css_frame_sh', 'compress:css_frame_sh'] },
            css_top_sh: { files: ['<%= paths.devsh %>_scss/*.scss'], tasks: ['compass:top_sh', 'csscomb:top_sh', 'copy:css_top_sh', 'compress:css_top_sh'] },
            css_info_sh: { files: ['<%= paths.devsh %>info/_scss/*.scss'], tasks: ['compass:info_sh', 'csscomb:info_sh', 'copy:css_info_sh', 'compress:css_info_sh'] },
            css_interior_sh: { files: ['<%= paths.devsh %>common/interior/_scss/*.scss'], tasks: ['compass:interior_sh', 'csscomb:interior_sh', 'copy:css_interior_sh', 'compress:css_interior_sh'] },
            css_oversea_sh: { files: ['<%= paths.devsh %>common/oversea/_scss/*.scss'], tasks: ['compass:oversea_sh', 'csscomb:oversea_sh', 'copy:css_oversea_sh', 'compress:css_oversea_sh'] },
            css_common_sh: { files: ['<%= paths.devsh %>common/_scss/*.scss'], tasks: ['compass:common_sh', 'csscomb:common_sh', 'copy:css_common_sh', 'compress:css_common_sh'] },
            css_ip_sh: { files: ['<%= paths.devsh %>common/ip/_scss/*.scss'], tasks: ['compass:ip_sh', 'csscomb:ip_sh', 'copy:css_ip_sh', 'compress:css_ip_sh'] },
            css_corp_sh: { files: ['<%= paths.devsh %>corp/_scss/*.scss'], tasks: ['compass:corp_sh', 'csscomb:corp_sh', 'copy:css_corp_sh', 'compress:css_corp_sh'] },
            css_genre_sh: { files: ['<%= paths.devsh %>genre/_scss/*.scss'], tasks: ['compass:genre_sh', 'csscomb:genre_sh', 'copy:css_genre_sh', 'compress:css_genre_sh'] },
            css_item_sh: { files: ['<%= paths.devsh %>item/_scss/*.scss'], tasks: ['compass:item_sh', 'csscomb:item_sh', 'copy:css_item_sh', 'compress:css_item_sh'] },
            css_blog_sh: { files: ['<%= paths.devsh %>item/blogger/_scss/*.scss'], tasks: ['compass:blog_sh', 'csscomb:blog_sh', 'copy:css_blog_sh', 'compress:css_blog_sh'] },
            css_ev_sh: { files: ['<%= paths.devsh %>event/_scss/*.scss'], tasks: ['compass:ev_sh', 'csscomb:ev_sh', 'copy:css_ev_sh', 'compress:css_ev_sh'] },

            /*csss_frame_sh:{files:['<%= paths.devsh %>frameworks/css/*.css'],tasks:['csscomb:frame_sh','copy:css_frame_sh','compress:css_frame_sh']},
            csss_top_sh:{files:['<%= paths.devsh %>css/*.css'],tasks:['csscomb:top_sh','copy:css_top_sh','compress:css_top_sh']},
            csss_info_sh:{files:['<%= paths.devsh %>info/css/*.css'],tasks:['csscomb:info_sh','copy:css_info_sh','compress:css_info_sh']},
            csss_interior_sh:{files:['<%= paths.devsh %>common/interior/css/*.css'],tasks:['csscomb:interior_sh','copy:css_interior_sh','compress:css_interior_sh']},
            csss_oversea_sh:{files:['<%= paths.devsh %>common/oversea/css/*.css'],tasks:['csscomb:oversea_sh','copy:css_oversea_sh','compress:css_oversea_sh']},
            csss_corp_sh:{files:['<%= paths.devsh %>corp/css/*.css'],tasks:['csscomb:corp_sh','copy:css_corp_sh','compress:css_corp_sh']},
            csss_genre_sh:{files:['<%= paths.devsh %>genre/css/*.css'],tasks:['csscomb:genre_sh','copy:css_genre_sh','compress:css_genre_sh']},
            csss_item_sh:{files:['<%= paths.devsh %>item/css/*.css'],tasks:['csscomb:item_sh','copy:css_item_sh','compress:css_item_sh']},
            csss_blog_sh:{files:['<%= paths.devsh %>item/blogger/css/*.css'],tasks:['csscomb:blog_sh','copy:css_blog_sh','compress:css_blog_sh']},
            csss_ev_sh:{files:['<%= paths.devsh %>event/css/*.css'],tasks:['csscomb:ev_sh','copy:css_ev_sh','compress:css_ev_sh']},*/

            js_frame_sh: { files: ['<%= paths.devsh %>frameworks/js/*.js'], tasks: ['copy:js_frame_sh', 'uglify:frame_sh', 'compress:js_frame_sh'] },
            js_top_sh: { files: ['<%= paths.devsh %>js/*.js'], tasks: ['copy:js_top_sh', 'uglify:top_sh', 'compress:js_top_sh'] },
            js_info_sh: { files: ['<%= paths.devsh %>info/js/*.js'], tasks: ['copy:js_info_sh', 'uglify:info_sh', 'compress:js_info_sh'] },
            js_interior_sh: { files: ['<%= paths.devsh %>common/interior/js/*.js'], tasks: ['copy:js_interior_sh', 'uglify:interior_sh', 'compress:js_interior_sh'] },
            js_oversea_sh: { files: ['<%= paths.devsh %>common/oversea/js/*.js'], tasks: ['copy:js_oversea_sh', 'uglify:oversea_sh', 'compress:js_oversea_sh'] },
            js_common_sh: { files: ['<%= paths.devsh %>common/js/*.js'], tasks: ['copy:js_common_sh', 'uglify:common_sh', 'compress:js_common_sh'] },
            js_ip_sh: { files: ['<%= paths.devsh %>common/ip/js/*.js'], tasks: ['copy:js_ip_sh', 'uglify:ip_sh', 'compress:js_ip_sh'] },
            js_corp_sh: { files: ['<%= paths.devsh %>corp/js/*.js'], tasks: ['copy:js_corp_sh', 'uglify:corp_sh', 'compress:js_corp_sh'] },
            js_genre_sh: { files: ['<%= paths.devsh %>genre/js/*.js'], tasks: ['copy:js_genre_sh', 'uglify:genre_sh', 'compress:js_genre_sh'] },
            js_item_sh: { files: ['<%= paths.devsh %>item/js/*.js'], tasks: ['copy:js_item_sh', 'uglify:item_sh', 'compress:js_item_sh'] },
            js_blog_sh: { files: ['<%= paths.devsh %>item/blogger/js/*.js'], tasks: ['copy:js_blog_sh', 'uglify:blog_sh', 'compress:js_blog_sh'] },
            js_ev_sh: { files: ['<%= paths.devsh %>event/js/*.js'], tasks: ['copy:js_ev_sh', 'uglify:ev_sh', 'compress:js_ev_sh'] },

            //pngスプライト画像生成、最適化
            /*png_top_sh: { files: ['<%= paths.devsh %>img/*.png'], tasks: ['sprite:top_sh', 'imageoptim:png_top_sh', 'copy:png_top_sh'] },
            png_top_header_sh: { files: ['<%= paths.devsh %>img/header/*.png'], tasks: ['sprite:top_header_sh', 'imageoptim:png_top_sh', 'copy:png_top_sh'] },
            png_top_footer_sh: { files: ['<%= paths.devsh %>img/footer/*.png'], tasks: ['sprite:top_footer_sh', 'imageoptim:png_top_sh', 'copy:png_top_sh'] },
            png_top_leftnav_sh: { files: ['<%= paths.devsh %>img/leftnav/*.png'], tasks: ['sprite:top_leftnav_sh', 'imageoptim:png_top_sh', 'copy:png_top_sh'] },
            png_top_sp_sh: { files: ['<%= paths.devsh %>img/sp/*.png'], tasks: ['sprite:top_sp_sh', 'imageoptim:png_top_sh', 'copy:png_top_sh'] },

            png_info_sh: { files: ['<%= paths.devsh %>info/img/*.png'], tasks: ['sprite:info_sh', 'imageoptim:png_info_sh', 'copy:png_info_sh'] },
            png_interior_sh: { files: ['<%= paths.devsh %>common/interior/img/*.png'], tasks: ['sprite:interior_sh', 'imageoptim:png_interior_sh', 'copy:png_interior_sh'] },
            png_oversea_sh: { files: ['<%= paths.devsh %>common/oversea/img/*.png'], tasks: ['sprite:oversea_sh', 'imageoptim:png_oversea_sh', 'copy:png_oversea_sh'] },
            png_common_sh: { files: ['<%= paths.devsh %>common/img/*.png'], tasks: ['sprite:common_sh', 'imageoptim:png_common_sh', 'copy:png_common_sh'] },
            png_ip_sh: { files: ['<%= paths.devsh %>common/ip/img/*.png'], tasks: ['sprite:ip_sh', 'imageoptim:png_ip_sh', 'copy:png_ip_sh'] },
            png_corp_sh: { files: ['<%= paths.devsh %>corp/img/*.png'], tasks: ['sprite:corp_sh', 'imageoptim:png_corp_sh', 'copy:png_corp_sh'] },
            png_genre_sh: { files: ['<%= paths.devsh %>genre/img/*.png'], tasks: ['sprite:genre_sh', 'imageoptim:png_genre_sh', 'copy:png_genre_sh'] },
            png_item_sh: { files: ['<%= paths.devsh %>item/img/*.png'], tasks: ['sprite:item_sh', 'imageoptim:png_item_sh', 'copy:png_item_sh'] },
            png_blog_sh: { files: ['<%= paths.devsh %>item/blogger/img/*.png'], tasks: ['sprite:blog_sh', 'imageoptim:png_blog_sh', 'copy:png_blog_sh'] },
            png_ev_sh: { files: ['<%= paths.devsh %>event/img/*.png'], tasks: ['sprite:ev_sh', 'imageoptim:png_ev_sh', 'copy:png_ev_sh'] },*/

            //jpg画像最適化
            jpg_top_sh: { files: ['<%= paths.devsh %>img/*.jpg'], tasks: ['imageoptim:jpg_top_sh', 'copy:jpg_top_sh'] },
            jpg_top_header_sh: { files: ['<%= paths.devsh %>img/header/*.jpg'], tasks: ['imageoptim:jpg_top_header_sh', 'copy:jpg_top_header_sh'] },
            jpg_top_footer_sh: { files: ['<%= paths.devsh %>img/footer/*.jpg'], tasks: ['imageoptim:jpg_top_footer_sh', 'copy:jpg_top_footer_sh'] },
            jpg_top_leftnav_sh: { files: ['<%= paths.devsh %>img/leftnav/*.jpg'], tasks: ['imageoptim:jpg_top_leftnav_sh', 'copy:jpg_top_leftnav_sh'] },

            jpg_info_sh: { files: ['<%= paths.devsh %>info/img/*.jpg'], tasks: ['imageoptim:jpg_info_sh', 'copy:jpg_info_sh'] },
            jpg_interior_sh: { files: ['<%= paths.devsh %>common/interior/img/*.jpg'], tasks: ['imageoptim:jpg_interior_sh', 'copy:jpg_interior_sh'] },
            jpg_oversea_sh: { files: ['<%= paths.devsh %>common/oversea/img/*.jpg'], tasks: ['imageoptim:jpg_oversea_sh', 'copy:jpg_oversea_sh'] },
            jpg_common_sh: { files: ['<%= paths.devsh %>common/img/*.jpg'], tasks: ['imageoptim:jpg_common_sh', 'copy:jpg_common_sh'] },
            jpg_ip_sh: { files: ['<%= paths.devsh %>common/ip/img/*.jpg'], tasks: ['imageoptim:jpg_ip_sh', 'copy:jpg_ip_sh'] },
            jpg_corp_sh: { files: ['<%= paths.devsh %>corp/img/*.jpg'], tasks: ['imageoptim:jpg_corp_sh', 'copy:jpg_corp_sh'] },
            jpg_genre_sh: { files: ['<%= paths.devsh %>genre/img/*.jpg'], tasks: ['imageoptim:jpg_genre_sh', 'copy:jpg_genre_sh'] },
            jpg_genre_month_sh: { files: ['<%= paths.devsh %>genre/month/img/*.jpg'], tasks: ['imageoptim:jpg_genre_month_sh', 'copy:jpg_genre_month_sh'] },
            jpg_item_sh: { files: ['<%= paths.devsh %>item/img/*.jpg'], tasks: ['imageoptim:jpg_item_sh', 'copy:jpg_item_sh'] },
            jpg_blog_sh: { files: ['<%= paths.devsh %>item/blogger/img/*.jpg'], tasks: ['imageoptim:jpg_blog_sh', 'copy:jpg_blog_sh'] },
            jpg_ev_sh: { files: ['<%= paths.devsh %>event/img/*.jpg'], tasks: ['imageoptim:jpg_ev_sh', 'copy:jpg_ev_sh'] },






            //yahoo
            html_top_ya: { files: ['<%= paths.devya %>*.html'], tasks: ['htmlmin:top_ya'] },
            html_info_ya: { files: ['<%= paths.devya %>info/*.html'], tasks: ['htmlmin:info_ya'] },
            html_interior_ya: { files: ['<%= paths.devya %>common/interior/*.html'], tasks: ['htmlmin:interior_ya'] },
            html_oversea_ya: { files: ['<%= paths.devya %>common/oversea/*.html'], tasks: ['htmlmin:oversea_ya'] },
            html_common_ya: { files: ['<%= paths.devya %>common/*.html'], tasks: ['htmlmin:common_ya'] },
            html_ip_ya: { files: ['<%= paths.devya %>common/ip/*.html'], tasks: ['htmlmin:ip_ya'] },
            html_corp_ya: { files: ['<%= paths.devya %>corp/*.html'], tasks: ['htmlmin:corp_ya'] },
            html_genre_ya: { files: ['<%= paths.devya %>genre/*.html'], tasks: ['htmlmin:genre_ya'] },
            html_genre_month_ya: { files: ['<%= paths.devya %>genre/month/*.html'], tasks: ['htmlmin:genre_month_ya'] },
            html_item_ya: { files: ['<%= paths.devya %>item/*.html'], tasks: ['htmlmin:item_ya'] },
            html_blog_ya: { files: ['<%= paths.devya %>item/blogger/*.html'], tasks: ['htmlmin:blog_ya'] },
            html_ev_ya: { files: ['<%= paths.devya %>event/*.html'], tasks: ['htmlmin:ev_ya'] },

            css_frame_ya: { files: ['<%= paths.devya %>frameworks/_scss/*.scss'], tasks: ['compass:frame_ya', 'csscomb:frame_ya', 'copy:css_frame_ya', 'compress:css_frame_ya'] },
            css_top_ya: { files: ['<%= paths.devya %>_scss/*.scss'], tasks: ['compass:top_ya', 'csscomb:top_ya', 'copy:css_top_ya', 'compress:css_top_ya'] },
            css_info_ya: { files: ['<%= paths.devya %>info/_scss/*.scss'], tasks: ['compass:info_ya', 'csscomb:info_ya', 'copy:css_info_ya', 'compress:css_info_ya'] },
            css_interior_ya: { files: ['<%= paths.devya %>common/interior/_scss/*.scss'], tasks: ['compass:interior_ya', 'csscomb:interior_ya', 'copy:css_interior_ya', 'compress:css_interior_ya'] },
            css_oversea_ya: { files: ['<%= paths.devya %>common/oversea/_scss/*.scss'], tasks: ['compass:oversea_ya', 'csscomb:oversea_ya', 'copy:css_oversea_ya', 'compress:css_oversea_ya'] },
            css_common_ya: { files: ['<%= paths.devya %>common/_scss/*.scss'], tasks: ['compass:common_ya', 'csscomb:common_ya', 'copy:css_common_ya', 'compress:css_common_ya'] },
            css_ip_ya: { files: ['<%= paths.devya %>common/ip/_scss/*.scss'], tasks: ['compass:ip_ya', 'csscomb:ip_ya', 'copy:css_ip_ya', 'compress:css_ip_ya'] },
            css_corp_ya: { files: ['<%= paths.devya %>corp/_scss/*.scss'], tasks: ['compass:corp_ya', 'csscomb:corp_ya', 'copy:css_corp_ya', 'compress:css_corp_ya'] },
            css_genre_ya: { files: ['<%= paths.devya %>genre/_scss/*.scss'], tasks: ['compass:genre_ya', 'csscomb:genre_ya', 'copy:css_genre_ya', 'compress:css_genre_ya'] },
            css_item_ya: { files: ['<%= paths.devya %>item/_scss/*.scss'], tasks: ['compass:item_ya', 'csscomb:item_ya', 'copy:css_item_ya', 'compress:css_item_ya'] },
            css_blog_ya: { files: ['<%= paths.devya %>item/blogger/_scss/*.scss'], tasks: ['compass:blog_ya', 'csscomb:blog_ya', 'copy:css_blog_ya', 'compress:css_blog_ya'] },
            css_ev_ya: { files: ['<%= paths.devya %>event/_scss/*.scss'], tasks: ['compass:ev_ya', 'csscomb:ev_ya', 'copy:css_ev_ya', 'compress:css_ev_ya'] },

            /*csss_frame_ya:{files:['<%= paths.devya %>frameworks/css/*.css'],tasks:['csscomb:frame_ya','copy:css_frame_ya','compress:css_frame_ya']},
            csss_top_ya:{files:['<%= paths.devya %>css/*.css'],tasks:['csscomb:top_ya','copy:css_top_ya','compress:css_top_ya']},
            csss_info_ya:{files:['<%= paths.devya %>info/css/*.css'],tasks:['csscomb:info_ya','copy:css_info_ya','compress:css_info_ya']},
            csss_interior_ya:{files:['<%= paths.devya %>common/interior/css/*.css'],tasks:['csscomb:interior_ya','copy:css_interior_ya','compress:css_interior_ya']},
            csss_oversea_ya:{files:['<%= paths.devya %>common/oversea/css/*.css'],tasks:['csscomb:oversea_ya','copy:css_oversea_ya','compress:css_oversea_ya']},
            csss_corp_ya:{files:['<%= paths.devya %>corp/css/*.css'],tasks:['csscomb:corp_ya','copy:css_corp_ya','compress:css_corp_ya']},
            csss_genre_ya:{files:['<%= paths.devya %>genre/css/*.css'],tasks:['csscomb:genre_ya','copy:css_genre_ya','compress:css_genre_ya']},
            csss_item_ya:{files:['<%= paths.devya %>item/css/*.css'],tasks:['csscomb:item_ya','copy:css_item_ya','compress:css_item_ya']},
            csss_blog_ya:{files:['<%= paths.devya %>item/blogger/css/*.css'],tasks:['csscomb:blog_ya','copy:css_blog_ya','compress:css_blog_ya']},
            csss_ev_ya:{files:['<%= paths.devya %>event/css/*.css'],tasks:['csscomb:ev_ya','copy:css_ev_ya','compress:css_ev_ya']},*/

            js_frame_ya: { files: ['<%= paths.devya %>frameworks/js/*.js'], tasks: ['copy:js_frame_ya', 'uglify:frame_ya', 'compress:js_frame_ya'] },
            js_top_ya: { files: ['<%= paths.devya %>js/*.js'], tasks: ['copy:js_top_ya', 'uglify:top_ya', 'compress:js_top_ya'] },
            js_info_ya: { files: ['<%= paths.devya %>info/js/*.js'], tasks: ['copy:js_info_ya', 'uglify:info_ya', 'compress:js_info_ya'] },
            js_interior_ya: { files: ['<%= paths.devya %>common/interior/js/*.js'], tasks: ['copy:js_interior_ya', 'uglify:interior_ya', 'compress:js_interior_ya'] },
            js_oversea_ya: { files: ['<%= paths.devya %>common/oversea/js/*.js'], tasks: ['copy:js_oversea_ya', 'uglify:oversea_ya', 'compress:js_oversea_ya'] },
            js_common_ya: { files: ['<%= paths.devya %>common/js/*.js'], tasks: ['copy:js_common_ya', 'uglify:common_ya', 'compress:js_common_ya'] },
            js_ip_ya: { files: ['<%= paths.devya %>common/ip/js/*.js'], tasks: ['copy:js_ip_ya', 'uglify:ip_ya', 'compress:js_ip_ya'] },
            js_corp_ya: { files: ['<%= paths.devya %>corp/js/*.js'], tasks: ['copy:js_corp_ya', 'uglify:corp_ya', 'compress:js_corp_ya'] },
            js_genre_ya: { files: ['<%= paths.devya %>genre/js/*.js'], tasks: ['copy:js_genre_ya', 'uglify:genre_ya', 'compress:js_genre_ya'] },
            js_item_ya: { files: ['<%= paths.devya %>item/js/*.js'], tasks: ['copy:js_item_ya', 'uglify:item_ya', 'compress:js_item_ya'] },
            js_blog_ya: { files: ['<%= paths.devya %>item/blogger/js/*.js'], tasks: ['copy:js_blog_ya', 'uglify:blog_ya', 'compress:js_blog_ya'] },
            js_ev_ya: { files: ['<%= paths.devya %>event/js/*.js'], tasks: ['copy:js_ev_ya', 'uglify:ev_ya', 'compress:js_ev_ya'] },


            //pngスプライト画像生成、最適化 yahoo
            /*png_top_ya: { files: ['<%= paths.devya %>img/*.png'], tasks: ['sprite:top_ya', 'imageoptim:png_top_ya', 'copy:png_top_ya'] },
            png_top_header_ya: { files: ['<%= paths.devya %>img/header/*.png'], tasks: ['sprite:top_header_ya', 'imageoptim:png_top_ya', 'copy:png_top_ya'] },
            png_top_footer_ya: { files: ['<%= paths.devya %>img/footer/*.png'], tasks: ['sprite:top_footer_ya', 'imageoptim:png_top_ya', 'copy:png_top_ya'] },
            png_top_leftnav_ya: { files: ['<%= paths.devya %>img/leftnav/*.png'], tasks: ['sprite:top_leftnav_ya', 'imageoptim:png_top_ya', 'copy:png_top_ya'] },

            png_info_ya: { files: ['<%= paths.devya %>info/img/*.png'], tasks: ['sprite:info_ya', 'imageoptim:png_info_ya', 'copy:png_info_ya'] },
            png_interior_ya: { files: ['<%= paths.devya %>common/interior/img/*.png'], tasks: ['sprite:interior_ya', 'imageoptim:png_interior_ya', 'copy:png_interior_ya'] },
            png_oversea_ya: { files: ['<%= paths.devya %>common/oversea/img/*.png'], tasks: ['sprite:oversea_ya', 'imageoptim:png_oversea_ya', 'copy:png_oversea_ya'] },
            png_common_ya: { files: ['<%= paths.devya %>common/img/*.png'], tasks: ['sprite:common_ya', 'imageoptim:png_common_ya', 'copy:png_common_ya'] },
            png_ip_ya: { files: ['<%= paths.devya %>common/ip/img/*.png'], tasks: ['sprite:ip_ya', 'imageoptim:png_ip_ya', 'copy:png_ip_ya'] },
            png_corp_ya: { files: ['<%= paths.devya %>corp/img/*.png'], tasks: ['sprite:corp_ya', 'imageoptim:png_corp_ya', 'copy:png_corp_ya'] },
            png_genre_ya: { files: ['<%= paths.devya %>genre/img/*.png'], tasks: ['sprite:genre_ya', 'imageoptim:png_genre_ya', 'copy:png_genre_ya'] },
            png_item_ya: { files: ['<%= paths.devya %>item/img/*.png'], tasks: ['sprite:item_ya', 'imageoptim:png_item_ya', 'copy:png_item_ya'] },
            png_blog_ya: { files: ['<%= paths.devya %>item/blogger/img/*.png'], tasks: ['sprite:blog_ya', 'imageoptim:png_blog_ya', 'copy:png_blog_ya'] },
            png_ev_ya: { files: ['<%= paths.devya %>event/img/*.png'], tasks: ['sprite:ev_ya', 'imageoptim:png_ev_ya', 'copy:png_ev_ya'] },*/

            //jpg画像最適化
            jpg_top_ya: { files: ['<%= paths.devya %>img/*.jpg'], tasks: ['imageoptim:jpg_top_ya', 'copy:jpg_top_ya'] },
            jpg_top_header_ya: { files: ['<%= paths.devya %>img/*.jpg'], tasks: ['imageoptim:jpg_top_header_ya', 'copy:jpg_top_header_ya'] },
            jpg_top_footer_ya: { files: ['<%= paths.devya %>img/*.jpg'], tasks: ['imageoptim:jpg_top_footer_ya', 'copy:jpg_top_footer_ya'] },
            jpg_top_leftnav_ya: { files: ['<%= paths.devya %>img/*.jpg'], tasks: ['imageoptim:jpg_top_leftnav_ya', 'copy:jpg_top_leftnav_ya'] },

            jpg_info_ya: { files: ['<%= paths.devya %>info/img/*.jpg'], tasks: ['imageoptim:jpg_info_ya', 'copy:jpg_info_ya'] },
            jpg_interior_ya: { files: ['<%= paths.devya %>common/interior/img/*.jpg'], tasks: ['imageoptim:jpg_interior_ya', 'copy:jpg_interior_ya'] },
            jpg_oversea_ya: { files: ['<%= paths.devya %>common/oversea/img/*.jpg'], tasks: ['imageoptim:jpg_oversea_ya', 'copy:jpg_oversea_ya'] },
            jpg_common_ya: { files: ['<%= paths.devya %>common/img/*.jpg'], tasks: ['imageoptim:jpg_common_ya', 'copy:jpg_common_ya'] },
            jpg_ip_ya: { files: ['<%= paths.devya %>common/ip/img/*.jpg'], tasks: ['imageoptim:jpg_ip_ya', 'copy:jpg_ip_ya'] },
            jpg_corp_ya: { files: ['<%= paths.devya %>corp/img/*.jpg'], tasks: ['imageoptim:jpg_corp_ya', 'copy:jpg_corp_ya'] },
            jpg_genre_ya: { files: ['<%= paths.devya %>genre/img/*.jpg'], tasks: ['imageoptim:jpg_genre_ya', 'copy:jpg_genre_ya'] },
            jpg_genre_month_ya: { files: ['<%= paths.devya %>genre/month/img/*.jpg'], tasks: ['imageoptim:jpg_genre_ya', 'copy:jpg_genre_month_ya'] },
            jpg_item_ya: { files: ['<%= paths.devya %>item/img/*.jpg'], tasks: ['imageoptim:jpg_item_ya', 'copy:jpg_item_ya'] },
            jpg_blog_ya: { files: ['<%= paths.devya %>item/blogger/img/*.jpg'], tasks: ['imageoptim:jpg_blog_ya', 'copy:jpg_blog_ya'] },
            jpg_ev_ya: { files: ['<%= paths.devya %>event/img/*.jpg'], tasks: ['imageoptim:jpg_ev_ya', 'copy:jpg_ev_ya'] },


            //全店舗共通ファイルの監視
            css_info_cmn: { files: ['<%= paths.devcmn %>info/_scss/*.scss'], tasks: ['compass:info_cmn', 'csscomb:info_cmn', 'copy:css_info_cmn', 'compress:css_info_cmn'] },
            html_info_cmn: { files: ['<%= paths.devcmn %>info/*.html'], tasks: ['htmlmin:info_cmn'] },
            js_info_cmn: { files: ['<%= paths.devcmn %>info/js/*.js'], tasks: ['copy:js_info_cmn', 'uglify:info_cmn', 'compress:js_info_cmn'] },
            png_info_cmn: { files: ['<%= paths.devcmn %>info/img/*.png'], tasks: ['sprite:info_cmn', 'imageoptim:png_info_cmn', 'copy:png_info_cmn'] },
            jpg_info_cmn: { files: ['<%= paths.devcmn %>info/img/*.jpg'], tasks: ['imageoptim:jpg_info_cmn', 'copy:jpg_info_cmn'] },

            css_cmn_cmn: { files: ['<%= paths.devcmn %>common/_scss/*.scss'], tasks: ['compass:cmn_cmn', 'csscomb:cmn_cmn', 'copy:css_cmn_cmn', 'compress:css_cmn_cmn'] },
            html_cmn_cmn: { files: ['<%= paths.devcmn %>common/*.html'], tasks: ['htmlmin:cmn_cmn'] },
            js_cmn_cmn: { files: ['<%= paths.devcmn %>common/js/*.js'], tasks: ['copy:js_cmn_cmn', 'uglify:cmn_cmn', 'compress:js_cmn_cmn'] },
            png_cmn_cmn: { files: ['<%= paths.devcmn %>common/img/*.png'], tasks: ['sprite:cmn_cmn', 'imageoptim:png_cmn_cmn', 'copy:png_cmn_cmn'] },
            jpg_cmn_cmn: { files: ['<%= paths.devcmn %>common/img/*.jpg'], tasks: ['imageoptim:jpg_cmn_cmn', 'copy:jpg_cmn_cmn'] }

            /*画像最適化
            jpg: {
                files: ['<%= paths.devrk %>item/blogger/img/*.jpg'],
                tasks: ['imageoptim:itemb_rk','copy:itemb_rk']
            },*/

        },
    });

    // 読み込むモジュールの設定
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-concat');
    //grunt.loadNpmTasks('grunt-ftpush');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-imageoptim');
    grunt.loadNpmTasks('grunt-ftp-deploy');
    grunt.loadNpmTasks('grunt-csscomb');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-styleguide');
    grunt.loadNpmTasks('grunt-contrib-clean');
    //grunt.loadNpmTasks('grunt-ect');

    // gruntコマンドで実行するタスクの設定
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('htmlYa', ['htmlmin:top_ya']);
    grunt.registerTask('cleanimg', ['clean:ipimages']);
    grunt.registerTask('imgpress_rk', ['imageoptim:jpg_rk']);
    grunt.registerTask('imgcopy_rk', ['copy:jpg_rk']);
};
