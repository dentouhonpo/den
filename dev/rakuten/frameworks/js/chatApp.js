var milkcocoa = new MilkCocoa("https://io-zi2kmw1x5.mlkcca.com/");
/* your-app-id にアプリ作成時に発行される"io-"から始まるapp-idを記入します */
var chatDataStore = milkcocoa.dataStore("chat");
var textArea, board;
window.onload = function() {
    textArea = document.getElementById("msg");
    board = document.getElementById("board");

    var txt1 = "在席中";
    var txt2 = "退席中"; 

    myTime = (new Date()).getHours();
    if (myTime >= 10 && myTime < 16) {
        document.getElementById("state").innerHTML = txt1;
    } else {
        document.getElementById("state").innerHTML = txt2;
    };
    document.getElementById("state").innerHTML = txt2;
}

function clickEvent() {
    var text = textArea.value;
    sendText(text);
}

function sendText(text) {
    chatDataStore.push({
        message: text
    }, function(data) {
        console.log("送信完了!");
        textArea.value = "";
    });
}

chatDataStore.on("push", function(data) {
    addText(data.value.message);
});

function addText(text) {
    var msgDom = document.createElement("li");
    msgDom.innerHTML = text;
    board.insertBefore(msgDom, board.firstChild);
}

var messageDataStore = milkcocoa.dataStore('chat').child('message');
console.log(messageDataStore);
