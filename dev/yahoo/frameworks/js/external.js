/*====================================================================
「prototype.js」
Sam Stephenson 氏作成の Ajax ライブラリの1つ
2008/01/25 現在 version 1.6.0.2
http://www.prototypejs.org/

prototype-1.6.0.2.js → prototype.js にリネイム

prototype.js の開発者向けメモ
バージョン 1.5.0 対応
http://www.imgsrc.co.jp/~kuriyama/prototype/prototype.js.html

prototype.jsのEvent.observeを使用
イベントに対して、イベントハンドラを追加する。
observe(element, name, observer, useCapture)
element: オブジェクトかID
name: イベント名 ('click', 'load' など)
observer: イベントを処理する関数 function(evt)
useCapture: true なら, capture 段階でイベントを処理し
            false なら bubbling 段階で処理する

http://hori-uchi.com/archives/000424.html

======================================================================*/

function externalLinks() {
 if (!document.getElementsByTagName) return;
 var anchors = document.getElementsByTagName("a");
 for (var i=0; i<anchors.length; i++) {
   var anchor = anchors[i];
   if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external") {
     anchor.target = "_top";
     anchor.onclick = function(event){return popupWindow(this, event);}
     anchor.onkeypress = function(event){return popupWindow(this, event);}
   }
 }
}

function popupWindow(anchor, event){
  var keyCode;
  if (event && event.type == 'keypress') {
    if (event.keyCode) {
      keyCode = event.keyCode;
    } else if (event.which) {
      keyCode = event.which;
    }
    // 13 == Return key. 32 == space key
    if (keyCode != 13 && keyCode != 32) {
      return true;
    }
  }
  return !window.open(anchor);
}

Event.observe(window,'load', externalLinks, false);
