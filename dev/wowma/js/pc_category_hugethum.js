var imgSize = 240; // サムネイルのサイズ指定 ※pxなど単位はつけないこと！
 
var j = jQuery.noConflict();
j( document ).ready(function(){
  j( 'div#risFil img[src$="fitin=128:128"]' ).each(function(){
    var thumImg = j(this);
    var replaceSrc = thumImg.attr( 'src' ).replace( '?fitin=128:128' , '?fitin=' + imgSize + ':' + imgSize );
    thumImg.attr( 'src' , replaceSrc );
  });
});