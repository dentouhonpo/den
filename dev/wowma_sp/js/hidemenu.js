// JavaScript Document
(function(){
function backscrollMenu(){
    var menuWrap = document.getElementById("spForm");
    var menuHeight = menuWrap.clientHeight;
    var before = 0;
    window.addEventListener("scroll", function(){
            var after = window.pageYOffset;
            if(after >= menuHeight) {
                    menuWrap.classList.add("transition");
            } else {
                    menuWrap.classList.remove("transition");
            }
            if(after > 200) {
                    if(after > before) {
                            menuWrap.style.top = "-" + menuHeight + "px";
                    }
                    else {
                            menuWrap.style.top = "0";
                    }
            } else {
                    menuWrap.style.top = "-" + menuHeight + "px";
            }
            before = after;
    });
}
if(document.getElementById('spForm')){
backscrollMenu();
}
})();