thisDay = new Date();
timeStamp = thisDay.getTime();
myMonth = thisDay.getMonth() + 1;
myDate  = thisDay.getDate();
myHours = thisDay.getHours();
myDay   = thisDay.getDay();

myWeekTbl = new Array( "日","月","火","水","木","金","土");

function isHoliday (year, month, date, nday) {
  nofw    = Math.floor((date - 1) / 7) + 10;
  shunbun = Math.floor(20.8431+0.242194*(year-1980)-Math.floor((year-1980)/4));
  syubun  = Math.floor(23.2488+0.242194*(year-1980)-Math.floor((year-1980)/4));
  if (month ==  1 && nday ==  1 && nofw ==  2) { return 1; } // 成人の日
  if (month ==  2 && date == 11)               { return 1; } // 建国記念の日
  if (month ==  3 && date == shunbun)          { return 1; } // 春分の日
  if (month ==  4 && date == 29)               { return 1; } // みどりの日
  if (month ==  5 && date >=  3 && date <=  5) { return 1; } // 憲法記念日～こどもの日
  if (month ==  7 && nday ==  1 && nofw ==  3) { return 1; } // 海の日
  if (month ==  9 && nday ==  1 && nofw ==  4) { return 1; } // 敬老の日
  if (month ==  9 && date == syubun)           { return 1; } // 秋分の日
  if (month ==  9 && nday ==  2 && nofw ==  3 
                  && date+1 == syubun) { return 1; } // 9月第3火曜の翌日が秋分の日→国民の休日
  /*if (month == 10 && date == 10) { return 1; }*/ // 体育の日
  if (month == 11 && date ==  3)               { return 1; } // 文化の日
  if (month == 11 && date == 23)               { return 1; } // 勤労感謝の日
  if (month == 12 && date == 23)               { return 1; } // 天皇誕生日
  if (nday  ==  0)                             { return 2; } // 日曜
  if (nday  ==  6)                             { return 2; } // 土曜

/* 毎年変更 ここから 2017 */

  //年始
  if (month ==  1 && date ==  1)               { return 1; } // 年始
  if (month ==  1 && date ==  2)               { return 1; } // 年始
  if (month ==  1 && date ==  3)               { return 1; } // 年始
  if (month ==  1 && date ==  8)               { return 1; } // 成人

  //GW
  if (month ==  5 && date ==  3)               { return 1; }  
  if (month ==  5 && date ==  4)               { return 1; } 
  if (month ==  5 && date ==  5)               { return 1; } 

  //社員研修 
  if (month ==  7 && date ==  10)               { return 1; } 
  if (month ==  7 && date ==  11)               { return 1; } 

  //海の日
  if (month ==  7 && date ==  17)               { return 1; } 
  
  //お盆
  if (month ==  8 && date ==  11)               { return 1; }
  if (month ==  8 && date ==  14)               { return 1; }
  if (month ==  8 && date ==  15)               { return 1; }
  if (month ==  8 && date ==  16)               { return 1; }

  //敬老の日
  if (month ==  9 && date ==  18)               { return 1; }

  //体育の日
  if (month ==  10 && date ==  9)               { return 1; }

  //年末
  if (month ==  12 && date ==  29)               { return 1; }
  if (month ==  12 && date ==  30)               { return 1; }
  if (month ==  12 && date ==  31)               { return 1; }

/* 毎年変更 ここまで */


  return 0;
}

function dispDateW () {
  return dispDate(1);
}

function dispDate1W (h) {
  return dispDate1(h, 1);
}

function dispDate2W (n, h) {
  return dispDate2(n, h, 1);
}

function dispDate (w) {
  return dateFormat(myMonth,myDate,myDay,w);
}

function dispDate1 (h, w) {
  return dispDate2(0, h, w);
}

function dispDate2 (n, h, w) {
  var i = 0;
  while (i <= n) {
    thisDay.setTime(timeStamp + (1000*60*60*24 * i));
    myYear2  = thisDay.getFullYear();
    myMonth2 = thisDay.getMonth() + 1;
    
    myDate2  = thisDay.getDate();
    myDay2   = thisDay.getDay();
    
    if (isHoliday(myYear2,myMonth2,myDate2,myDay2) == 0 && i == 0 && h <= myHours) { n++; }
    if (isHoliday(myYear2,myMonth2,myDate2,myDay2) >= 1){ n++; } //µÙÆ?
    if (isHoliday(myYear2,myMonth2,myDate2,myDay2) == 1 && myDay2 == 0){ n++; }
    i++;
  }
  return dateFormat(myMonth2,myDate2,myDay2,w);
}

//Display
function dateFormat (month, date, week, w) {
  //if (w == 1) { return month + "/"+ date + "(<span>"+myWeekTbl[week]+")</span>" ; }

  if (w == 1) { return month+"月"+date+"日"; }
  //if (w == 1) { return "8·?18Æ?" ; }
  else { return month+"月"+date+"日"; }
}

