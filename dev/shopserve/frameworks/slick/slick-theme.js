/* slick iframe用スライド設定 */
$(document).on('ready', function() {
    $(".center").slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        /*centerMode: true,*/
        fade: false,
        /*initialSlide: 2,*/
        cssEase: 'linear',
        swipeToSlide:true,
        touchMove:true,
        touchThreshold:1000,
        edgeFriction:0,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,

            }
        }]
    });
});
