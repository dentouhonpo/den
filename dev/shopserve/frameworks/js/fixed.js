if ((navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
    // ページの読み込みが完全に完了したら以下の処理を実行
    window.onload = function() {　　
        // 「#top-bar」を固定/解除するための基準となる値を取得し、
        // 変数「topbar」に代入
        var topbar = $("#top").offset().top - $("#bghack").height();
        //var bottombar =
        //$("#qalist").offset().top - $("#footer_bar").height();

        // 画面がスクロールされたら以下の処理を実行
        $(window).scroll(function() {　　　　
            // スクロールトップの位置が「topbar」よりも値が大きければ、
            　　　　 // 「#top-bar」を固定
            if ($(window).scrollTop() > topbar)
                $(".mean-bar").css({
                    "position": "fixed",
                    "top": "0px",
                    "left": "0px;",
                    "z-index": "100",
                    "border-bottom": "1px solid #e84925"
                }),
                $("#fixIcon li").css({
                    "padding": "1%"
                });
            // 小さければ、「#top-bar」の固定を解除
            else　 $("#fixIcon").css({
                "position": "static",
                "border-bottom": "none"
            })
        });
    }
}
