$(function() {
    $(".genrecontent:not('.genreactive + .genrecontent')").hide();
    $(".genremenu").hover(function() {
        $(this).addClass("genrehover")
    }, function() {
        $(this).removeClass("genrehover")
    });
    $(".genremenu").click(function() {
        $(".genremenu").removeClass("genreactive");
        $(this).addClass("genreactive");
        $(".genrecontent:not('.genreactive + .genrecontent')").fadeOut();
        $(".genreactive + .genrecontent").fadeIn();
    });
});
$(function() {
    $(".eventcontent:not('.eventactive + .eventcontent')").hide();
    $(".eventmenu").hover(function() {
        $(this).addClass("eventhover")
    }, function() {
        $(this).removeClass("eventhover")
    });
    $(".eventmenu").click(function() {
        $(".eventmenu").removeClass("eventactive");
        $(this).addClass("eventactive");
        $(".eventcontent:not('.eventactive + .eventcontent')").fadeOut();
        $(".eventactive + .eventcontent").fadeIn();
    });
});
jQuery(document).ready(function() {
    var slideNum = $('.slide').size();
    $('#slider').bxSlider({
        auto: true,
        pager: true,
        speed: 1500,
        slideWidth: 990,
        moveSlides: 1,
        onSliderLoad: function(currentIndex) {
            $('.slide').removeClass('active');
            $('#slider > div:nth-child(3n-1)').addClass('active');
        },
        onSlideBefore: function($slideElement, oldIndex, newIndex) {
            var new_i = newIndex % 3 - 1;
            var nth = (new_i < 0) ? '3n-1' : '3n' + new_i;
            $('.slide').removeClass('active');
            $('#slider > div:nth-child(' + nth + ')').addClass('active');
        }
    });
});
