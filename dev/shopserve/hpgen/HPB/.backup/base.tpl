<!DOCTYPE html>

<html lang="ja" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<title>{block name="pageTitle"}{literal}和食器・陶器・九谷焼の専門店 | 九谷焼 伝統本舗{/literal}{/block}</title>
<meta name="description" content="{literal}九谷焼の専門店で納得の通販。10,000品目以上の中から厳選した作品から 和食器・陶器 九谷焼 を販売。贈り物やお祝いにオススメ!{/literal}">
<meta name="keywords" content="{literal}九谷焼,贈り物,ギフト{/literal}" />
<meta name="robots" content="noindex">
{block name="linkCss"}<link href="{literal}/vol1blog/d/dentou.or.shopserve.jp{/literal}/docs/hpgen/HPB/theme/css/default.css" rel="stylesheet" type="text/css" />{/block}
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://dentou.or.shopserve.jp/hpgen/HPB/rss.xml" />
{$overture}
{literal}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
$(function($) {
    var topBtn = $('#page-top');
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});
</script>

{/literal}



<!--[if lt IE 9]>
<script src="https://ie7-js.googlecode.com/svn/trunk/lib/IE9.js"></script>
<![endif]-->

<style type="text/css">
</style>

{block name="pageJs"}{* ・冷#・ン・ャヮ蜚・*}{/block}
</head>
<body>
{block name="mainHeader"}{* ・冷#・ン・ャヮ蜚・*}{/block}
    <div id="wrapper">
    <div id="cart-header" >
    {literal}
      <SCRIPT type="text/javascript">
      <!--
      function search_back(){
        var isMSIE = /*@cc_on!@*/false; 
        if (isMSIE) {
          if(document.charset!="utf-8"){
            document.charset="utf-8";
          }
        }
      }
      function openPage(url, form){
        var linkerUrl = url;
{/literal}{if isset($googleData) && $googleData.gAnalytics != ""}{literal}
        var clickUrl      = '/_ga/' + url.replace(/https:\/\//,"");
        _gaq.push(['_trackPageview',{ page : clickUrl }]);
        _gaq.push(function() {
          var pageTracker = _gat._getTrackerByName();
          linkerUrl = pageTracker._getLinkerUrl(url);
        });
{/literal}{/if}{literal}
        window.open(linkerUrl,form,'width=600,height=580,scrollbars=yes');
        return false;
      }
      //-->
      </SCRIPT>
      <SCRIPT type="text/javascript">
      <!--
      function goCart(){
        document.CART.REFERER.value = document.URL;
        document.CART.TIME.value    = (new Date()).getTime();
{/literal}{if isset($googleData) && $googleData.gAnalytics != ""}{literal}
        _gaq.push(['_trackPageview',{ page : '/_ga/cart.php' }]);
        _gaq.push(['_linkByPost', document.CART]);
{/literal}{/if}{literal}
        document.CART.submit();
      }
      //-->
      </SCRIPT>
    {/literal}
    <form method="POST" action="https://cart8.shopserve.jp/dentou.or/cart.php" name="CART">
        <input type="hidden" name="REFERER" value=""/>
        <input type="hidden" name="TIME" value=""/>
    </form>
    {literal}
    <div id="header-inner">
    <!-- TITLE-AREA BEGIN -->
    <div id="logo">
         <h1>
                    </h1>
        
            </div>
    <div id="hNav">
                <div class="cart-nav">
        <ul id="header-nav">
                      <li id="header-nav-mypage">
                   <a class="header-mypage" onClick="javascript:openPage('https://cart8.shopserve.jp/dentou.or/login.cgi','mypage');
return false;" href="/_ga/login.cgi" title="マイページへログイン">マイページへログイン</a></li>
                                <li id="header-nav-cart">
                   <a class="header-cart" onClick="javascript:goCart();return false;" href="/_ga/cart.php" title="カートをみる"
>カートをみる</a></li>
                  </ul>
        </div>
    </div>
    <!-- TITLE-AREA END -->
</div>
          <div id="gnav">
                <div class="cart-nav">
                  <ul>
            <li><a href="https://dentou.or.shopserve.jp" title="TOP">TOP</a></li>
                        <li><a href="https://dentou.or.shopserve.jp/hpgen/HPB/shop/shoppinguide.html" title="ご利用案内" >ご利用案内</a></li>
                                    <li><a onClick="javascript:openPage('https://cart8.shopserve.jp/dentou.or/FORM/contact.cgi','form'); return false;" href="/_ga/FORM/contact.cgi" title="お問い合せ">お問い合せ</a></li>
                                                <li><a href="https://dentou.or.shopserve.jp/hpgen/HPB/shop/sitemap.html" title="サイトマップ">サイトマップ</a></li>
                      </ul>
                </div>
              </div>
    {/literal}
    </div>
          <div id="contents">
      <div id="formArea" class="cart-article">
        <div id="main-column-full">
            <div id="mainContents_wrap">
                <div id="bread-crumb">
                    <div id="bread-crumb-listTop" class="cart-nav">
                        <ol>
                          <li><a href="https://dentou.or.shopserve.jp" title="{literal}TOP{/literal}" >{literal}TOP{/literal}</a></li>
                          <li>{literal}ショッピングカート{/literal}</li>
                        </ol>
                    </div>
                </div>
                                <div class="cart-section" >
                    <div id="contact">
                        <!-- SHOP-PAGE CONTENT-AREA BEGIN -->
                        {$main}
                        <!-- SHOP-PAGE CONTENT-AREA END -->
                    </div>

                    
                                        {literal}
                    <SCRIPT type="text/javascript">
                    <!--
                    var ref;
                    ref = document.referrer;
                    ref = ref.replace(/&/ig,"%26");
                    ref = ref.replace(/\?/ig,"%3F");
                    var u = document.URL.replace(/&/ig,"%26").replace(/</ig,"%3C").replace(/\'/ig,"%27");
                    document.write("<img src='https://b.shopserve.jp/tracking/tracking.php?");
                    document.write("U="+u+"&S="+document.domain+"&W="+screen.width+"&H="+screen.height+"&");
                    document.write("V=50177&C={/literal}{$cmd|escape|escape:javascript|default:''}{literal}&R="+ref+"' width=1 height=1>");
                    // -->
                    </SCRIPT>
                    {/literal}
                </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <!--FOOTER AREA-->
    <div id="cart-footer" >
        {literal}
        <div id="footer-inner">
  <div class="footer_gnavi">
    <ul>
            <li><a href="https://dentou.or.shopserve.jp/hpgen/HPB/shop/policy.html" title="個人情報の取り扱いについて">個人情報の取り扱いについて</a></li>
                  <li><a href="https://dentou.or.shopserve.jp/hpgen/HPB/shop/business.html" title="特定商取引法に関する表示">特定商取引法に関する表示</a></li>
                </ul>
  </div>
  <div id="copyright">
    <ul>
      <li></li>
    </ul>
  </div>
</div>
        {/literal}
    </div>
    <p id="page-top"><a href="#wrapper" title="ページトップへ"><img src="/vol1blog/d/dentou.or.shopserve.jp/docs/hpgen/HPB/theme/img/pagetop.gif" alt="ページトップへ" /></a></p>
    {$remarketingtag}
    </body>
</html>
