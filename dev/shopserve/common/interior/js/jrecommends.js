var count = 0x3EDBF;//203185
var cnt = count.toString(10);
var items = [];
var itemUrl = [];
var names = [];
var k = 0;

$.ajaxSetup({scriptCharset:'euc'});

function mycarousel_itemLoadCallback(carousel, state){
    if (state != 'init')
       return;

    $.getJSON("https://ashiato.rakuten.co.jp/rms/sd/ashiato/vc?act=6&sid=1&callback=?", function(data) {
       mycarousel_itemAddCallback(carousel, carousel.first, carousel.last, data);
    });
};

function mycarousel_itemAddCallback(carousel, first, last, data){
    for(var j in data.items){
        if(data.items[j].shopid == cnt){
            items[k] = data.items[j].imageurl64;
            itemUrl[k] = data.items[j].itemurlfull;
            names[k] = data.items[j].itemname;
            k += 1;
        }
    }

    var innerdata = "";
    if (k == 0){
       names[0] = "お客様が最近チェックした商品";
       innerdata += '<DIV style="height:140px;width:950px;position:absolute; top:0px; left:0px;background:#e6eced;"><p class="txt02">' + names[0] + '</p></div>';
       carousel.add(0, innerdata);
       carousel.size(names.length);
    }else{
       for (i = 0; i < items.length; i++) {
            innerdata = '<A href="' + itemUrl[i] +  '" target="_top">';
            innerdata += '<img class="img01" src="' + items[i] + '" width="80" alt="" /></a>';
            innerdata += '<A href="' + itemUrl[i] +  '" target="_top">';
            innerdata += '<p class="txt01">' + names[i] + '</p></a>';
            carousel.add(i+1, innerdata);
       }
       carousel.size(items.length);
    }
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        auto: 0,
        visible: 6,
        size: 0,
        scroll: 6,
        easing: 'easeOutBack',
        itemLoadCallback: mycarousel_itemLoadCallback
    });
});

$(window).unload(function(){
  window.location.reload(false);
});